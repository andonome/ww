\index{Health}
\section{Health and Injury}

Each character has a Health trait, comprising seven levels that reflect increasingly severe injuries.
The ease and permanence of reductions to Health depend on the type of damage inflicted and the nature of the character.
Some supernatural beings are more resilient to injury than humans, but they may still be hurt, incapacitated or even destroyed.

\label{healthChart}
\healthChart

Each character sheet contains a Health chart to allow damage to the individual to be tracked and its effects on their actions assessed.
Each box after the first has an associated dice-pool penalty that increases with the severity of the wounds.
As the character's condition worsens, it becomes harder to carry out actions.
Each point of damage inflicted on the character (after soaking) reduces his health levels by one.
The first point of damage, Bruised, has little effect on the character's actions, but penalties mount until the seventh point, Incapacitated, at which point the character is unable to carry out any actions.

Damage is marked off on the Health chart in different manners depending the nature of the wound, since some types of injury are easier and quicker to heal than others. (See ``Applying
Damage'', p. \label{applyingDamage}).
The lowest checked box denotes the character's current health level.
The number to the left of the box indicates the current dice-pool penalty.
This penalty is assessed on all dice pools save those associated with reflexive actions (such as soak) as long as the wound remains.
Injury also impedes a character's movement as noted on the accompanying chart.
Characters with no checked boxes are in full health and suffer no penalties.
Those with all boxes checked are Incapacitated -- either unconscious or otherwise unable to act.

\index{Damage}
\subsection{Applying Damage}
\label{applyingDamage}

When characters gain penalties to physical actions, it represents damage to the body.
Each damage type is indicated on the same chart, but in a slightly different way.
\ifvamp
  Bashing and lethal damage are marked with a slash (\slashedBox), and aggravated damage is marked with two (\lethalBox).
\else
  Bashing damage is marked with a slash (\slashedBox), lethal damage with an X (\lethalBox), and aggravated damage with three slashes (\aggBox).
\fi
This is done because the different wound types heal at different rates.
For the same reason, aggravated wounds are always recorded ``highest'' (that is with the lowest wound penalty) on the chart, followed by lethal wounds and finally with bashing wounds.
This is done by ``moving down'' bashing and lethal wounds as appropriate.
This allows the gravest health levels to be healed first.

\ifvamp
  \begin{example}[Example:]
    Anna faces off against an angry woodsman who has had enough of her talking.
    Karsh, the woodsman, first pushes Anna away, slamming her into a nearby tree.
    This causes two levels of bashing damage, so Anna's player marks off the Bruised and Hurt levels with slashes, like so:
    Anna needs to hold the woodsman's attention so her friends can escape, however, so Karsh picks up his large ax and lays into her.
    The blow does one level of lethal damage, so Anna's player adds a bar to the Bruised box on his chart (turning the slash into an X) and puts a slash through the Injured box, hence moving the existing two levels of bashing damage down the chart, like so:
    
    Finally, Karsh goes over the edge and reveals his supernatural nature.
    His hands become large, vicious claws, which he then uses to tear into Anna's flesh.
    The attack causes two levels of aggravated damage, so the player turns the marks in Bruised and Hurt to asterisks, the one in
    Injured to an X, and puts slashes in Wounded and Mauled (see below).
    Anna player now suffers a -2 dice penalty to all actions, and can only hobble a few yards per turn.
    Thankfully she was given a potion by an old woman who told her it had healing properties.
    She can only hope the woman wasn't mad\ldots
  \end{example}
\fi

\subsection{Damage Types}
\label{damageTypes}

Characters in \setting\ can suffer injury from a broad range of sources.
It may be bruising from punches and kicks, broken bones from a mace or slashes from a sword.
A character's physical nature determines her susceptibility to injury and the degree to which she is affected.

\subsubsection{Bashing Damage}

Bashing damage is unlikely to kill, and it can be healed swiftly.
It represents bruises, sprains and similar minor injuries, inflicted by falls, punches and the like.
\ifvamp
  Vampires suffer far less than mortals from these kinds of blunt blows, and so reduce all incoming Bashing Damage by half (round down).

  Mortals
\else
  Everyone
\fi
can reduce bashing damage by 1, so if they receive a -4 penalty due to bashing damage, they receive 3 damage (and a -3 penalty) instead.

While Bashing damage rarely kills, it can weaken the character to a point where she is more susceptible to other forms of damage.
Once the Health chart is full, additional levels of bashing damage cause existing bashing slashes to be replaced with lethal crosses, as bones are broken and internal organs rupture.

\subsubsection{Lethal Damage}

As the name suggests, lethal damage is more serious than bashing with the potential to kill or maim.
Most bladed weapons inflict lethal damage, as do some blunt weapons if targeted appropriately (+1 difficulty).
Any such character at Incapacitated who suffers a level of lethal damage dies.

\subsubsection{Aggravated Damage}

Aggravated sources of injury are largely elemental or supernatural in origin (e.g., fire, the teeth and claws of supernatural beings, magical weapons).
\ifvamp
  Vampires are particularly susceptible to aggravated damage, and fear it instinctively (see page \pageref{rotschreck}).
\fi

\section{Recovery}

Recovering from a Contest happens in different ways.
Physical penalties almost universally take the forms of bodily damage, which heals over time.
Other forms of recovery can be less predictable, and must be left to Storyteller fiat.

A good rule of thumb is to allow players to recovery from minor blemishes against their reputation between sessions.
If someone has been publicly embarrassed at \ifvamp Elysium\else\ifDA the recent harvest festival\else the local club\fi\fi, they can remove a single penalty after any prolonged downtime.

Mental Contests usually keep their penalties forever.
Once an investigation goes dead, it's dead; but an investigation which uncovers a lot of information won't go away -- in fact the information can go public, meaning any time someone attempts to find out a character's crime, that character begins with the same penalty.

If someone has clearly lost a Mental Contest, the only way to continue is finding a completely different approach.
When you can't show someone is a murderer, you can still investigate other crimes, or if someone has embezzled funds but the Intelligence + \ifDA Seneschal \else Finance \fi Arena has proven fruitless, it may still be possible to show infiltrate their organization with Intelligence + Etiquette.

\subsection{Healing}
\label{healing}

The following indicates the length of time required for a human character to heal her injuries.
Each level must be healed individually, so someone who has reached Incapacitated through Bashing damage will require 12 hours to heal to the Crippled level, and a further six hours to recover to Mauled.

\index{Damage!Bashing}
\subsubsection{Bashing Damage}

Bashing damage is largely superficial, and it will heal without treatment.
Healing bashing damage does not require the player to make a roll.
The main factor in such healing is time, though the application of poultices and compresses can alleviate their effects and speed healing. (For details on doing so, see page \pageref{healing}.)

\index{Health!Recovery}
\begin{boxTable}[XX]{Bashing Recovery Times}

\textbf{Health Level} & \textbf{Recovery Time} \\

1-3 levels	& One hour/level \\
4 levels			& Three hours \\
5 levels		& Six hours \\
Incapacitated		& Twelve hours \\

\end{boxTable}

\index{Damage!Lethal}
\subsubsection{Lethal Damage}

Lethal damage is just that, and it is often fatal.
Even minor wounds may become infected and thus become life-threatening, while scars are commonplace.

Characters with levels of lethal damage below Hurt who do not receive medical aid, automatically suffer an additional level of lethal damage per day, reflecting blood loss and infection.
After initial medical attention, the ministrations of a healer are not required, provided the character is at Mauled or better.
Characters at Crippled or Incapacitated require constant medical care and are usually delirious or unconscious.

Whenever a character seeks to heal a lethal health level, the player should roll a number of dice equal to her Stamina score.
The difficulty of this roll is 2 + the number of Health chart boxes crossed (e.g., a character whose Wounded level is the lowest box crossed has four boxes crossed, so the difficulty is 6).
On one or more successes, that level is healed.
If no successes occur, the character's situation remains unchanged.
On a botch result, the character suffers a new lethal wound and might (if already at Incapacitated) die as a result.
\ifDA
  The presence of a trained healer may reduce this difficulty, but only in Arabic lands is medicine sufficiently advanced to have an appreciable effect.
\fi

A human character at the Incapacitated level who takes an additional wound (bashing or lethal) dies immediately.

\index{Health!Recovery}
\begin{boxTable}[XX]{Lethal Recovery Times}

\textbf{Health Level}	& \textbf{Recovery Time} \\
1         & One day \\
2     		& Three days \\
3        	& One week \\
4        	& One month \\
5       	& Six months \\

\end{boxTable}
\label{healingChart}

\ifvamp
  \input{vampire/7-drama.tex}
  \input{vampire/7-drama-health.tex}
\fi

\subsection{Soaking Damage}

When characters receive injury from impersonal sources (lightning, falling, et c., but not other characters), they can try to mitigate some of it.

The exact mitigations depend upon the sources.
Avoiding smoke inhalation might use Stamina, and limiting damage from a fall would normally employ Dexterity.
At the Storyteller's option, some soaking rolls could add an Ability.

Damage from fights or other Resisted rolls do not normally employ soak rolls -- each character is already resisting damage with the traits employed after all.

\subsection{Other Sources of Injury}

Though violent physical injury is prevalent in the \ifDA Dark Medieval\else World of Darkness\fi, there are many other threats to the average person's life that are less overt and often more deadly.

\index{Health!Disease}
\subsubsection{Disease}

\ifvamp
There are certain advantages to being a walking corpse.
  One of the biggest is a natural immunity to most diseases.
  \ifDA Leprosy, Ergotism, \else Cancer, AIDS,\fi influenza and other illnesses mean little or nothing to the undead.
  
  But immunity to disease doesn't mean the vampires can ignore diseases.
  Any illness that can be transmitted by the blood is a potential problem for vampires, because they can carry the illness and transmit it from victim to victim.
  \ifDA\else
  Indeed, several Kindred in Haiti and the US have become active carriers for the HIV virus.
  By drinking from someone infected with the
  HIV virus and then feeding on different victims, these vampires have helped to spread an already rampant infection.
  
  In some fiefdoms any vampire found carrying HIV is locked away for the good of the herd.
  In rare cases such carriers have even been put to Final Death for spreading the disease.
  Such plague-dogs are frowned upon heavily in the Camarilla, for not only does disease threaten the human populace, but victims of the disease might speak of their affiliation with vampires, putting the Masquerade in grave danger.
  
  Vampires with the Medicine Knowledge are sometimes recruited by the primogen in major cities to regulate the spread of disease through the Kiss.
  In the past decade, such vampires have been invited to speak before conclaves, alerting elder and neonate alike about noticeable signs of drug abuse and obvious physical symptoms that vampires should try to avoid.
  Even the vampires of the Sabbat, with their lack of concern for the herd, have begun to consider regulations regarding disease carriers.
  \fi
  
  An Intelligence + Medicine roll (difficulty 7) will allow characters to detect the presence of Plague, hepatitis or other blood-related diseases.
  If the roll is failed, the vampire does not notice the symptoms and exposes himself to disease (Stamina roll, difficulty 6, to avoid).
  A botch indicates the character feeds sloppily and automatically becomes a carrier for the disease.
  
  Kindred legends speak of certain plagues potent enough to affect vampires.
  Very few vampires have any knowledge of such ailments, and those who do are highly prized.
  Despite the Kindred's formidable powers, they are ill prepared to handle the occasional illness that can cause them harm.

\fi

\ifDA
  Disease is a major factor of medieval life.
  There are a wide variety of diseases present in the Dark Medieval, of which the most common are:
\fi

\paragraph{Dysentery (The Flux):}
Caused by contaminated water and poor hygiene, dysentery causes gastrointestinal problems in the victim, including bleeding, bowel pains, fever and dehydration.

\ifDA
  \paragraph{Ergotism:}
  A result of fungal poisoning (ergot, growing on rye, and thus possibly used in bread), ergotism causes agonizing muscle pains, convulsions, hallucinations and often loss of limbs as a result of gangrene.

  \paragraph{Idropesie (Dropsy):}
  Dropsy is the result of a weak heart that is unable to pump blood efficiently.
  
  \paragraph{Leprosy:}
  Leprosy is one of the most significant diseases of the Dark Medieval.
  It is a degeneration of the nerves, which reduces feeling and exposes the victim to a host of secondary infections that can harm the skin and damage bones, leading to disfigurement and mutilation.
  It is rarely fatal, but those infected with the disease are cast out of normal society, forced to wear distinct clothing and carry a bell to warn others.
  
  \paragraph{Plague:}
  Various plagues (bubonic, pneumonic and septicemic) permeate the Dark Medieval, becoming mass epidemics in the mid 14th century, devastating the population of Europe.
  The mortality rate of plague is 50-80 percent, with the period of illness characterized by fever and delirium.
\fi

\paragraph{Syphilis:}
Spread by sexual intercourse (or the transmission of blood), syphilis manifests initially as a fever and minor aches and pains.
Eventually the signs become apparent in the form of ulcers and bad skin, though the worst damage is internal, damaging bones, muscles and the brain.

\paragraph{Tuberculosis:}
The greatest killer of the Dark Medieval, this disease can affect any part of the body, though the lungs are the most commonly affected organs.
The disease causes a fever and sweating, and also leads to emaciation and tissue destruction.
The symptoms lead to the alternative name for the disease: consumption.

\ifDA\else
\index{Damage!Electricity}
\subsubsection{Electrocution}

  \ifvamp
    Vampires are not nearly so affected by simple electricity as are mortals.
    Nonetheless, electrocution might occasionally prove a danger.
  \fi
  The strength of the electrical flow determines the amount of lethal damage a character takes from electrocution.
  She suffers the damage effect noted below each turn until contact with the source is severed (Strength roll to pull away -- difficulty \ifvamp 5 for vampires, 9 for mortals\else 9\fi).
  \ifvamp
  Vampires may soak this damage normally -- but, if a soak roll is botched, the damage is considered aggravated, as the vampire's bloodstream and brain are fried.
  \fi
  
  Electrical damage is a lethal effect, and armor doesn't protect against it (depending on the subject's defenses, the circumstance and the Storyteller's decision).
  
  \begin{boxTable}[lX]{}
  Health Levels/Turn & Electrical Source \\
  
  One & Minor; wall socket \\
  Two & Major; protective fence \\
  Three & Severe; vehicle battery, junction box \\
  Four & Fatal; main feed line, subway rail \\
  
  \end{boxTable}
  
  If a character is subjected to significant amounts of electrical damage (chat reduce her to Incapacitated), she may suffer permanent damage.
  This can be physical impairment (reduced Physical Attributes), permanent memory loss, brain damage (reduced Mental Attributes) or disfigurement (reduced Appearance).
  Specifics are up to the Storyteller.
\fi

\ifvamp
\subsubsection{Temperature Extremes}
  Vampires, being undead, suffer little from the privations of temperature.
  However, high (200°F+) temperatures might have the same effects as fire, at the Storyteller's discretion.
  Vampires suffering from extreme cold might be forced to spend additional blood points or suffer from the effects of frostbite (-1 or more to Dexterity-based dice pools).
  In general, though, vampires should not suffer greatly from most ``normal'' temperature fluctuations.
\fi

\index{Damage!Falling}
\subsubsection{Falling}

For every 10 feet a character falls, she suffers one level of bashing damage that may be soaked normally with Stamina.
This damage may not exceed 10 levels, but if it reaches that level, it is treated as lethal rather than bashing, and the soak value of armor is halved (round down).
Likewise, if the surface on which the character lands has sharp edges (such as spikes) the Storyteller may opt to inflict some or all of this damage as lethal rather than bashing.

\index{Damage!Drowning}
\subsubsection{Drowning or Suffocation}

Characters are vulnerable to drowning if they botch a swimming roll, cannot swim or are weighed down.
The length of time a character can survive without air depends on her Stamina.
After this period has elapsed, she loses one health level in bashing damage per turn.
Once her health chart reaches Incapacitated, the character has drowned, and she dies after a number of minutes equal to her Stamina rating.

\begin{sideBox}[Breath Duration]

\begin{traitList}
  \trait{30 seconds}%
  \trait{One minute}%
  \trait{Two minutes}%
  \trait{Four minutes}%
  \trait{Eight minutes}%
\end{traitList}

\end{sideBox}

\subsubsection{Fire}
\label{fire}

\ifDA
  Fire is ever present in the Dark Medieval, most often as cooking fires, braziers and torches.
  It causes aggravated damage and ignores all armor protection.
  Indeed, metal armor exacerbates the situation by retaining heat.
  A character whose clothes are on fire, or who is trapped in a fire, continues to take damage until she escapes or extinguishes the flames.
\fi

  \index{Damage!Fire}
  \begin{boxTable}[lX]{Fire Effects}
    \textbf{Wounds/Turn} & \textbf{Fire Type} \\
    One & Torch; a part of the body is exposed to flame \\
    Two & Bonfire; half of the body is exposed to flame \\
    Three & Raging inferno; entire body is engulfed in \\
  \end{boxTable}

\ifvamp

  Vampires fear fire, for it is one of the few things that can end their immortal existences.
  Fire damage is aggravated and ignores armour; it may be soaked only with Fortitude.
  A fire's size determines the levels of aggravated damage a character endures per turn, while its heat determines the difficulty of the Fortitude soak roll.
  A character suffers the full damage effect for each turn that she's in contact with the flames; she must leave the area and/ or put out any fire on her to stop taking damage.
  All damage inflicted by fire is automatically successful unless soaked (i.e., a character trapped in a bonfire takes two automatic health levels of damage per turn, not the results of two damage dice per turn).
  
  \begin{boxTable}[lX]{}
  \textbf{Soak Difficulty} & \textbf{Heat of Fire} \\
  3  & Heat of a candle (first-degree burns) \\
  5  & Heat of a torch (second-degree burns) \\
  7  & Heat of a Bunsen burner (third-degree bums) \\
  8  & Heat of an electrical fire \\
  9  & Heat of a chemical fire \\
  10 &  Molten metal \\
  \end{boxTable}
\fi

If your character takes a -2 Penalty, she is scarred temporarily by the flames (reduce Appearance by one until her wounds recover to Bruised).
If she takes a -3 or -4 Penalty, the burns cover the majority of her body, reducing Appearance by two.

Characters losing dots in Appearance may also lose the use of Fame, if they are sufficiently disfigured.

\subsubsection{Drugs and Poisons}
\index{Poisons}\index{Drugs}
\label{poisons}\label{drugs}

\ifvamp
  \input{drama/poisons-vampire.tex}
\else
  \input{drama/poisons.tex}
\fi


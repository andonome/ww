\section{Physical Feats}

\index{Climbing}
\paragraph{Climbing [Dexterity + Athletics]:}
Scaling vertical surfaces, be they cliff faces or walls, is a staple of those who walk the night.
For a character to climb a surface, the player must roll
Dexterity + Athletics (difficulty 6).
This is an extended roll, with each success indicating that the character moves up 10 feet or so.
The Storyteller may adjust this distance depending on the difficulty of the climb (perhaps 15 feet per success for an easy slope with lots of handholds or 5 feet per success for a more challenging ascent across a tightly bonded wall).
Failure indicates that the character is currently unable to progress on the climb, perhaps unable to find a suitable route or needing to adjust her position before proceeding.
A botch, however, can prove disastrous.
At the Storyteller's discretion, the climber may become stuck, panic as a result of the height, or fall.

\ifDA
  \index{Archery}
  \paragraph{Archery [Dexterity + Archery]:}
  Shooting a target is a Resisted roll.
  The shooter rolls, and each success becomes a single point of Lethal Damage, inflicting a -1 penalty.

  If the target is aware of the attempt, the best thing to do is move fast -- preferably towards cover.
  The basic difficulty to shoot something standing right in front of you is 4, but a great many factors can add to this difficulty.
  Generally, someone being shot will run towards the archer, or to safety.
  In this case, the Storyteller will stipulate the number of successes required on a Dexterity + Athletics roll to reach the  target.
  \archeryChart

  Specially crafted bows can deal 1 additional Damage (assuming they hit), but they require Strength 3 to pull back.
  \ifvamp
    There are legends of Cainite master craftsmen who can craft stronger bows, but they require super-human strength to pull back.
  \fi

  \label{mountedCombat}
  \paragraph{Mounted Combat [Wits + Ride]:}
  Any half-able rider on a battlefield can circle about and plan a good time for a charge.
  Anyone seeing a giant animal galloping towards them makes a Willpower roll (standard difficulty 6, but varies) not to flee.
  And even better, riders do not usually get caught on the wrong foot, as someone else is taking care of the footing.
  Once the rider moves away, any difficulty penalties they suffer from poor positioning vanish, so this is a strong position for someone who has a better raw dice pool than their opponents.

\else
  \index{Driving}
  \paragraph{Driving [Dexterity/Wits + Drive]:}
  A Drive roll isn't needed to steer a vehicle under normal circumstances -- assuming your character has at least one dot in the Drive Skill.
  Bad weather, the vehicle's speed, obstacles and complex maneuvers can challenge even the most competent drivers.
  Specific difficulties based on these circumstances are up to the Storyteller, but should increase as the conditions become more hazardous.
  
  For example, driving in heavy rain is +1 difficulty, but going fast over difficult terrain increases the difficulty to +3.
  A failed roll indicates trouble, requiring an additional roll to avoid crashing or losing control.
  Characters in control of a vehicle, and who have no dots in the appropriate Ability, need a roll for almost every change in course or procedure.
  On a botch, the vehicle may spin out of control or worse.

  Because different cars handle differently -- some are designed for speed and handling while others are designed for safety -- a chart is provided to help calculate the difficulty for any maneuver.
  Generally, for every 10 miles over the safe driving speed of a vehicle, the difficulty of any maneuver is increased by one.
  Exceedingly challenging stunts and bad road conditions should also increase the difficulty accordingly.
  The maximum number of dice a driver can have in her dice pool when driving is equal to the maneuver rating of the vehicle.
  Simply put, even the best driver will have more trouble with a dump truck than she will with a Ferrari.

  \index{Trucks}
  \index{Cars}
  \index{Racing}
  \begin{boxTable}[Xccc]{}
  \textbf{Vehicle} & \textbf{Safe Speed} & \textbf{Max Speed} & \textbf{Maneuver} \\
  6-Wheel Truck & 60 & 90 & 3 \\
  Tank (modern) & 60 & 100 & 4 \\
  Tank (WWII) & 30 & 40 & 3 \\
  Bus & 60 & 100 & 3 \\
  18-Wheeler & 70 & 110 & 4 \\
  Sedan & 70 & 120 & 5 \\
  Minivan & 70 & 120 & 6 \\
  Compact & 70 & 130 & 6 \\
  Sporty Compact & 100 & 140 & 7 \\
  Sport Coupe & 110 & 150 & 8 \\
  Sports Car & 110 & 160 & 8 \\
  Exotic Car & 130 & 190+ & 9 \\
  Luxury Sedan & 85 & 155 & 7 \\
  Midsize & 75 & 125 & 6 \\
  SUV & 70 & 115 & 6 \\
  Formula One Racer & 140 & 240 1& 0 \\

  \end{boxTable}

  \paragraph{Drive Chases}
  Pursuing someone means starting a Contest with the Drive Skill.
  The target may select the Perception Attribute to spot places where pursuit would prove challenging, or the Wits Attribute, to take fast turns, and pick all the routes where heavy traffic could block anyone following up.

  \subparagraph{Penalties} inflicted on a driver might indicate falling behind, or damage to the vehicle as they bash into other vehicles while crossing traffic.

  \subparagraph{Difficulties} represent challenges to keeping up, such as oil chucked on the car's front screen, or a police presence, liable to catch anyone speeding to catch up to another.
\fi

\paragraph{Jumping [Strength + Athletics]:}
Jumping is a simple task, and short leaps may be made without any die rolls.
Longer leaps or those in which a mishap may occur, are rolled against a difficulty of 3.
Standing jumps use the character's Strength, while running jumps use Strength + Athletics.
Each success allows the character to jump up to two feet vertically or four feet horizontally.
A failed roll indicates that the character failed to clear the required distance, though the player may make a Dexterity + Athletics roll to grab onto a ledge or otherwise avoid injury as the character falls.
On a botch, the character fails in her attempt to jump, either landing short and injuring herself in the fall or overshooting and slamming into an obstacle, with similar consequences.
Characters may attempt to judge the distance required for the leap (and thus the number of successes required).
To do so, the player needs three successes on a Perception + Athletics roll (difficulty 6).

\paragraph{Opening/Closing [Strength]:}
Forcing open a locked door with brute force requires a Strength roll (difficulty 6).
The number of successes required depends on the type of door and fastening.
A single success suffices for the simple latched door of a cottage, while two or three successes are needed for a more solid door or one secured by a bolt.
A barred door in a castle requires five or more successes.
At the Storyteller's discretion, the attempts to open the door can be an extended action, the accumulation of successes reflecting increasing damage to the door or its frame.

\ifDA
  Teamwork may also be used for forcing a door, particularly in the case of large portals such as castle gate (which may have a difficulty of 8 and require 10 or more successes to force).
\else
  A vault door might take 10 or more successes.
These successes may be handled as an extended action.
\fi
On a botched roll, the character takes one level of bashing damage as her shoulder or legs suffer under the battering.

\paragraph{Pursuit [Dexterity + Athletics]:}
\ifvamp
  Vampires must often pursue their terrified prey, and sometimes they themselves must flee.
\fi%
The type of ground determines the difficulty of the Contest.
Flat, open streets are difficulty 3, while a forest in the darkness is less predictable territory, so even the most skilled runner can simply run afoul of some obstacle.

\subparagraph{Penalties} represent ground lost, pure and simple.
If someone starts the chase farther away, they also begin with more successes.

\subparagraph{Difficulties} could represent darting \ifDA through a crowd and getting lost\else across a highway just before the traffic starts to pick up\fi.

As with any Contest, the Arena can change, so the evading character might ensure a drawn-out run to tire someone out, switching the Attributes to Stamina, or either could jump in a car, switching the Ability to Drive (even if only one is in a car, both use drive, but only one gains the Bonus dice).

\ifvamp
  For chases involving vampires and mortals, remember that mortals tire, but the undead do not.
\fi

\paragraph{Swimming [Stamina + Athletics]:}
To swim, a character must have at least one dot of Athletics.
Most routine swims do not require any form of roll, but those involving long distances or difficult conditions do.
In most cases, the target for this Stamina + Athletics roll is 5, but this can be increased one or two points for difficult circumstances.
The specific circumstances determine just when a roll is needed: Long-distance swimming requires a roll after an hour and only one success is necessary to remain afloat, though the difficulty of such rolls increases by one every hour. (This assumes that the character swims at a slow, steady pace of one or two miles per hour.
Characters who attempt to swim faster tire more quickly and thus should roll every few hundred yards).
Characters in difficult conditions (e.g., a raging river or a rip current) should make a roll every 10 yards.
Failure to get any successes doesn't mean the character drowns, rather that she begins to have difficulty and perhaps lose ground.
Only on a botch does a character begin to drown.

\paragraph{Throwing [Dexterity + Athletics]:}
Small objects (those weighing three pounds or less) can be thrown a number of yards equal to 5 x Strength.
For every additional two pounds, the distance an object can be hurled decreases by five yards.
Streamlined or irregularly shaped objects may travel greater or lesser distances, at the Storyteller's discretion.
Throwing an object with any degree of accuracy requires a Dexterity + Athletics roll with a difficulty of 6 (at half or less the potential range) or 8 (half to full range).
The size of the target area and the object's characteristics may also modify the difficulty, as may environmental factors such as wind and light.
One or more successes indicate that the object lands where it was intended, while failure indicates that the object misses its target area.
A botch may be anything from dropping the object (perhaps on the thrower's own toes) to hitting an ally, depending on the circumstances.

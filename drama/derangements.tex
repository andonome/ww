\subsection{Derangements}
\label{derangements}

 A character's mental state is as important as her physical health, and it is much less controllable in many cases.
Some characters have these quirks at the start of play.
Others accumulate them during game play at the Storyteller's discretion, usually after periods of intense stress, terror or anxiety.

Derangements are not intended to straitjacket a character, although they unquestionably create challenges for the player and her companions to solve.
They also provide opportunities for dramatic roleplaying and storytelling.
\ifDA
Madness is a very real part of the Dark Medieval, and those who suffer from it are variously seen as cursed or blessed.
\fi%
In many cases, they respond to outside stimuli in a manner that makes perfect sense to them but is odd to outsiders who do not comprehend their frame of reference.
Derangements should not be arbitrary in their effect, but linked to their own internally consistent set of rules.
Most often these rules are linked to the origins of the character's derangement and should be agreed upon by the player and Storyteller.
For example, a fear of flames might come from a character being tortured with fire by an inquisitor.
Some characters have such derangements at the start of play.
Others accumulate them during game play at the Storyteller's discretion, usually after periods of intense stress, terror or anxiety.

\ifvamp Vampires or mortals\else Characters\fi receive derangements under conditions of intense terror, guilt or anxiety.
\ifvamp%
  If a player botches a Virtue or Willpower roll (for example, when confronted with R\"otschreck), the Storyteller may decide that the experience causes a derangement in the character.
  Other examples of derangement-inducing events include killing a loved one while in a frenzy, being buried alive, or seeing hundreds of years of careful scheming dashed in an instant of bad luck.
\else
  The system effects of derangements vary from case to case.
  They usually result from having experienced a truly traumatic event.
  When appropriate, the Storyteller can ask a deranged character's player to roll Willpower to resist a derangement overcoming the character when presented with a relevant stimulus.
\fi

It must be noted that people who are ``crazy'' are neither funny nor arbitrary in their actions.
Insanity is frightening to those who are watching someone rage against unseen presences or hoard rotten meat to feed to the monsters that live next door; even something as harmless-sounding as talking to an invisible rabbit can become disturbing to observers.
The insane, however, are only responding to a pattern known to them, stimuli that they perceive in their own minds.
To their skewed perceptions, what's happening to them is perfectly normal -- to them.

Your character's derangement is there for a reason\ifvamp, whether he's a Malkavian who resided at Bedlam before his Embrace or a Ventrue who escaped from five months of torture at the hands of an Inquisitor\fi.
What stimuli is his insanity inflicting on him, and how is he reacting to what's happening?
The player should work with his Storyteller to create a pattern of provocations for his derangement, and then decide how his character reacts to such provocation.

Derangements are a challenge to roleplay, without question, but a little time and care can result in an experience that is dramatic for all involved.

\paragraph{Amnesia:}
Amnesiac characters 
have trouble remembering some things, and learning new skills.
The reasons for this memory loss are almost always stress-related, though physical injury may cause a similar effect.

Any time they botch a roll involving an Ability, they lose that Ability temporarily, along with all memories strongly associated with it.
The memories resurface once they both a different roll, and lose another Ability (so they lose access to one Ability at all times).

A lost Ability cannot be increased with Experience Points.

\paragraph{Bulimia:}

Individuals with bulimia assuage their guilt and insecurity by indulging in activities that comfort them -- in this case, consuming food.
A bulimic will eat tremendous amounts of food when subjected to stress, then empty her stomach through drastic measures so she can eat still more.

\ifvamp
  In the case of vampires with this derangement, the need to feed is a means of relieving the fear and anxiety endemic to the
  \setting.
  A bulimic vampire may feed four or more times a night -- gorging herself, burning the blood in pointless (or not so pointless) activity, then starting the cycle again.

  A vampire with bulimia gets hungry much more quickly than other vampires do.
  When feeding, a bulimic vampire must make a Conscience roll (difficulty 7).
  If she fails the roll, she feeds until her blood pool is full, whether the vampire needs the extra blood or not.
  A vampire who is forcibly kept from feeding risks frenzy (make a frenzy roll, difficulty 6).
  The difficulty increases by one for every 15 minutes that she is prevented from drinking.
\fi

\paragraph{Catatonia:}
A character suffering from this derangement may withdraw from the world entirely at times of stress, remaining largely immobile and unresponsive.
Due to the major limiting factors on their actions, catatonia is not recommended as a player derangement.

\paragraph{Fantasy:}
Some characters cannot accept the real world, so they transpose themselves into an illusory world instead.
The scope and degree of this fantasy varies considerably.
A character may hold conversations with characters who aren't there, or hear ``voices from the gods'' commanding him to carry out a wide range of acts.
He may also interact with people and institutions of the \setting\ in an almost normal manner but with their perception of events skewed by their fantasy.
For example, a character may regard himself as Lancelot from the Arthurian legends, on a quest to slay a vile demon or rescue a fair princess.
Such fantasies manifest as a quirky outlook, but they are rarely dangerous.
They can, however, adversely affect the character's reaction to others, perhaps making them more likely to carry out a mad assault (``The grail lies this way!'').

During Contests, the character will frame the contest in some way which accords with their delusion.
However, any change to the Arena (for example, changing a Wits + Academics contest into Intelligence + Academics) will inflict a -2 penalty to the character on the next round, as they need time to reframe the scene to accord with their fantasy.

\paragraph{Hysteria:}
Hysterical characters are unable to control their emotions, and they suffer from severe mood swings.

Whenever the character fails a Social Contest, they must make a \ifvamp Self-Control roll (difficulty 7) \else Willpower roll (difficulty 8) \fi or switch to a Physical Contest of some kind -- either fleeing immediately, or attacking.

\paragraph{Lunacy:}
This madness comes and goes, linked to the cycle of the moon.
When the moon is full, the character is manic and delirious, then lulls into a depressed state afterwards.
At other times, he may appear normal and unaffected.

During the three nights of the full moon, the first time the character sees the moon they regain all Willpower.
Thereafter, they must lose 2 Willpower each scene.
If they don't spend the 2 Willpower points, they will simply be lost.

\paragraph{Megalomania:}
Characters with this derangement believe that they are destined to lead, and they seek to accumulate power, irrespective of their skill and the attitude of others.
They believe that those who dismiss their claims are jealous, seeking to hold onto power and deny them their dues.
The megalomaniacs believe that these opponents should be destroyed, politically or physically.

These people tend to view every interaction as some kind of competition, and the Storyteller will present every social setting in the form of a Contest, whether or not it really is.
The player can then choose to ignore the contest (`who cares what this idiot thinks?'), and potentially suffer the consequences, or respond in kind (which immediately begins an actual Social Contest, even if nobody wanted to act against the character).

\paragraph{Manic-Depression:}

Manic-depressives suffer from severe mood swings, sometimes resulting from severe trauma or anxiety.
Victims may be upbeat and confident one moment, then uncontrollably lethargic and pessimistic the next.

Characters with this afflictions can only gain Willpower, and never spend it\ldots until their Willpower is full.
Then the script flips, and they may only spend Willpower, but never gain any, until they have no Willpower points left, and the script flips again.

\paragraph{Melancholia:}
Characters with this derangement frequently slip into deep depression, losing interest in their normal activities and becoming withdrawn.
These depressive periods often follow failure of a particular action, though they may also result from other psychological factors.

Characters with this affliction must keep at least 5 Willpower points at all times.
If they go under this threshold, they risk melancholia; any time they enter a Social or Mental Contest and fail a single roll, they immediately lose the Contest, along with 1 Willpower Point.

\paragraph{Obsessive/Compulsive:}

The trauma, guilt or inner conflict that causes this derangement forces the individual to focus nearly all other attention and energy onto a single repetitive behavior or action.
Obsession relates to an individual's desire to control her environment - keeping clean, keeping an area quiet and peaceful, or keeping undesirable individuals from an area, for example.
A compulsion is an action or set of actions that an individual is driven to perform to soothe her anxieties: for example, placing objects in an exact order, or feeding from a mortal in a precise, ritualistic fashion that is never allowed to vary.

Any time the character cannot fulfil their compulsive behaviour, they suffer +1 difficulty to all Social and Mental actions.
\ifvamp
  The difficulty of any attempt to coerce or Dominate a vampire into ceasing her behavior is raised by one.
  If a vampire is forcibly prevented from adhering to her derangement, must make a Self-Control roll (difficulty 9) to avoid Frenzy.
\fi

\ifvamp
\paragraph{Sanguinary Animism:}

  This derangement is unique to the Kindred, a response to vampires' deep-seated guilt regarding the act of feeding on the blood of mortals.
  Kindred with this derangement believe that they do not merely consume victims' blood, but their souls as well, which are then made a part of the vampire's consciousness.
  In the hours after feeding, the vampire hears the voice of her victim inside her head and feels a tirade of ``memories'' from the victim's mind -- all created by the vampire's subconscious.
  In extreme cases, this sense of possession can drive a Kindred to carry out actions on behalf other victims.
  Obviously, diablerie would be unwise for an animist to perform\ldots 
  
  Whenever a vampire with this derangement feeds on a mortal, a Willpower roll is needed (difficulty 6, or 9 if she drains the mortal to the point of death).
  If the roll succeeds, she is tormented by the `memories' of the person whose soul she has partially consumed, but is still able to function normally.
  If the roll fails, then the images in her mind are so strong that it is akin to having a second personality inside her, an angry and reproachful personality that seeks to cause harm to the vampire and her associates.
  Any time the kindred takes an action outside of a Contest, the Ability is tainted by another flood of memories and accusations, and receives +1 difficulty for the remainder of the night.
  The first action carries no penalty, but the vampire must avoid using the same Ability twice when not fully engaged with an interlocutor.
  The next day, all memories fade, and all penalties vanish.
\fi

\paragraph{Schizophrenia:}

Everything is something, everything is meaningful; schizophrenic characters will never fail any roll to uncover information\ldots or at least, they will never experience failure, they will only sense something else.
If they look for someone following them, they will see distant shadows.
If they listen for anyone else awake in the hospital, they will hear voices talking in the room next door.
If they check for supernatural influences in their mind then they \emph{will} find them.

Whenever the player makes a roll to uncover information, the Storyteller makes a secret roll in opposition.
If something exists to be uncovered, the player is informed as usual.
If there is nothing there, the Storyteller fills in something plausible for the environment.

Many characters can overcome the effects of their condition by simply checking with someone else, or examining hard, physical evidence.
However, characters with excellent senses or a keen insights ironically suffer from delusions all the more because they are the best source of information they know.

\ifDA
  \paragraph{Saint Vitus's Dance:}
  Technically a disease of the nervous system (known in the 21st century as Sydenham's chorea) rather than a derangement, the effects of Saint Vitus's Dance are thought of as a form of madness by the inhabitants of the Dark Medieval.
  It causes involuntary movements of the face and limbs, resulting in a dance-like series of movements that persist for days or weeks and then disappear, sometimes permanently but often reappearing after months or years.
  Often a result of rheumatic fever, Saint Vitus's Dance can spread throughout a group, leading to mass outbreaks of the ``madness''.
  
  \paragraph{Visions:}
  This derangement leads sufferers to believe that they are granted an insight into the divine through visions, trances and other ecstatic states.
  During these spells, they may be catatonic, in a trance-like state or rave uncontrollably.
  Their ability to recall details of the visions is similarly varied, sometimes recalling precise details while having only the vaguest recollections of others.
  Some details may not resurface until days after the revelation, emerging in response to some external stimulus.
  These visions may be products of an overactive imagination, or they could, at the Storyteller's discretion and very rarely, reflect a real insight into the unknown.

  Characters who roll four dice of the same number receive a religious vision, and become locked into that roll.%
  \footnote{One the first Visions set in, there are no more.}
  All further rolls of the same type receive a -1 difficulty bonus, while any other rolls receive a +1 difficulty.
  The character can try to `snap out of it', at the end of each scene with a Willpower roll (difficulty 8).
\fi

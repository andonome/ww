\subsection{Social Endeavors}

These systems cover tasks involving the three Social Attributes (Appearance, Manipulation and Charisma).

\begin{sideBox}[Rolling before Rollplay]

  Dice should never replace roleplaying, but they can \textit{guide} it.
  Any time you enter a tricky social situation, whether it's a Contest or a standard action, the dice should inform the actions.

  If you roll a botch, consider what the most awkward thing someone in your situation might say to \ifvamp an elder\else \ifDA a baron\else a police officer\fi\fi.
  If you roll three successes, act casual.
  With more successes, hand it back to the Storyteller and ask a question of any characters around, or ask her to suggest something fitting for the situation.

\end{sideBox}

\ifDA
  \paragraph{Buying and Selling [Manipulation + Commerce]:}
  Although coins are found all across Europe, barter remains the cornerstone of the Dark Medieval economy.
  Markets and fairs rather than shops are the most common sources of goods (though craftsmen and specialists may well sell from permanent facilities), with farmers arriving at town gates around dawn and completing their business during daylight hours.
  
  Finding common goods in the markets is a straightforward process, which usually takes only a few minutes.
  Less common items may require a Perception + Commerce roll, the difficulty depending on the commonality of the item being sought.
  Haggling is the main means of buying and selling goods, particularly when bartering other goods and services rather than using cash.
  Haggling occurs via resisted Manipulation + Commerce rolls, with the winner receiving a discount proportionate to the number of successes she achieves.
  One success equates to roughly 10 percent, while three successes drives the merchant down to her minimum asking price.
  Five successes allow the buyer to talk the merchant into accepting a loss.

  \paragraph{Gathering Rumors [Variable]:}
  News is important to the denizens of the Dark Medieval, and it is often exchanged in taverns and markets, each of which is a hotbed of local gossip and an exchange for stories from further afield.
  Hearing news is a simple matter, requiring a Charisma +
  Alertness roll, the successes indicating the number and detail of the stories.
  Sorting out truth from fiction is more difficult, requiring a roll of Perception paired with whichever Ability best covers the subject of conversations.
  Commerce, Etiquette and Politics are common choices, but not the only ones.
  Common knowledge is easy to obtain (difficulty 3) but more jealously guarded information is challenging or difficult to come by (difficulty 7 or 8).
  Of course, a local market (such as at Kings
  Lynn) is less likely to provide news from across Europe than is a major regional fair (such as those at Troyes or Lübeck).
  
  \paragraph{Managing a Household or Business [Intelligence + Seneschal]:}
  Managing a business or a major household (such as a lord's retinue) requires considerable acumen and skill to balance the books and keep things operating at peak efficiency.
  Managing day-to-day affairs requires a weekly Intelligence + Seneschal roll, the difficulty depending on the complexity of the business/ household and the local economic affairs.
  Managing day-to-day affairs in a small house in Lincoln is difficulty 4 -- largely routine in all but the most strenuous economic circumstances -- while managing the king's fortress in Paris, the
  Louvre, is at least 7, even in ideal circumstances.

  Hiring staff requires a Perception + Empathy roll, the difficulty linked to the number of available workers.
  One success is adequate help, three is good, and five brings in exceptional talent that is an asset to the business or household.
  Retaining these staff (and keeping them honest) is part of the regular routine, but where misdeeds occur, handling them requires Charisma + Leadership.

  Keeping track of finances, stock levels and related items requires a Perception + Seneschal roll (perhaps Perception + Investigation to track down any misdemeanors).
  
  Managing relationships with the local craft and trade associations (called variously guilds, communes and corporations) is often as important as the day-to-day running of the house.
  Doing so requires a weekly Manipulation + Seneschal roll.
  Characters who are senior figures in the guild also have to deal with the secular (nobility) and spiritual (clergy) authorities, neither of whom are allowed membership.
  Dealing with them entails Intelligence + Politics rolls.
  It should be noted that although Dark Medieval society is riven with gender discrimination, women are accepted as a vital part of business, either working alongside their husbands or as owner-operators in their own right.
\fi

\paragraph{The Poison Tongue [Perception/ Manipulation + Etiquette\ifDA/ Seneschal\else/Politics\fi]:}
Just as finding allies is critical, turning opinion against a political rival is also often of crucial importance.
Doing so requires a Manipulation + Politics roll, with a difficulty determined by the degree of competence and political aptitude demonstrated by one's opponent.
Everyone can turn opinion against the court bumbler, but throwing darts at the hero of the hour without seeming petty is very difficult.
The number of successes determines the degree of effect.
One success plants doubts, five successes changes opinions forever.
Note that it can take dozens of manipulations to really effect a change of opinion in the court as a whole.
A campaign of whispered slander and insinuation takes months of hard effort to bring to maturation.
\ifDA
  A botch during this time probably makes the matter into a public feud or even brings about a challenge to a duel.
\fi

\subparagraph{Penalties} represent straight-up tarnished reputations.
The target might have been manipulated into \ifvamp insulting the Prince\else getting obscenely drunk publicly\fi, or everyone has been convinced they \ifDA blasphemed against the local church\else said horrible things on the internet\fi.

\subparagraph{Difficulties} suggest mounting suspicion, or perhaps just being cut off from the local \ifvamp Elysium\else social circle\fi, leaving them less able to defend their name.

Sometimes, whispered insinuations aren't enough.
Framing someone for a crime she didn't commit is always a popular way to eliminate an enemy, so the Arena may switch over to Manipulation + Subterfuge\ifDA, or Seneschal\fi.
It is particularly effective if she is suspected of committing that sort of crime but hasn't been caught yet.
Setting the matter up requires the player to make an Intelligence + Subterfuge roll, with a difficulty based to the legitimacy of the victim.
The actual false accusation should be roleplayed out, and many other systems are likely to be put into use in the process.

\paragraph{Carousing [Charisma + Empathy]:}
You influence others (particularly potential vessels) to relax and have fun.
This might include showing a potential ally a good time, loosening an informant's tongue or making instant drinking partners who come to your aid when a brawl starts.
The difficulty is typically 6 (most people can be persuaded to loosen up, regardless of intellect or will), though it might be higher in the case of large (or surly) groups.
Certain Natures (Bon Vivant, Curmudgeon) can also influence the roll's difficulty.
On a botch, your character comes off as an obnoxious boor, or people begin to question why your character hasn't touched her own food and drink\ldots 

\paragraph{Credibility [Manipulation/Perception + Subterfuge]:}
The Subterfuge Talent is used with Manipulation when perpetrating a scam or with Perception when trying to detect one (a scam can range from impersonating the authorities to using forged papers).
All parties involved, whether detecting the lie or perpetrating it, make an appropriate roll (typically difficulty 7).
The seam's ``marks'' must roll higher than the perpetrator to detect any deception.
False credentials and other convincing props may add to the difficulty of uncovering the dupe, while teamwork may help reveal the scam.
\ifDA\else Hacking and/or intrusion rolls may be called for to pull off an inspired scam successfully.\fi
If your character perpetrates the scam and you botch, the entire plan falls apart.

\paragraph{Interrogation [Manipulation + Empathy/Subterfuge]:}
Anyone can ask questions.
With the Interrogation Ability, you ask questions and have leverage.
Interrogating someone peacefully (Manipulation + Empathy) involves asking strategic questions designed to reveal specific facts.
This method is a resisted action between your character's Manipulation + Empathy and the subject's Willpower.
Both actions are typically made against a difficulty of 6.
Rolls are made at key points during questioning, probably every few minutes or at the end of an interrogation session.

Violent interrogation (Manipulation + Subterfuge) involves torturing the victim's mind and/or body until she reveals what she knows.

\ifDA
  \paragraph{Oration:}
  [Charisma + Expression/Leadership]: Whether one is addressing the court or inspiring an army, oration is a vital skill in the Dark Medieval.
  The ability to manipulate other people through the use and delivery of words is a major tool in the arsenal of politicians, generals and the clergy.
  When a character seeks to sway others in this manner, roll Charisma + either
  Expression or Leadership.
  Use the former if the character is enchanting the crowd and the latter for efforts to rouse a crowd to action.
  The difficulty of this roll is usually 6, but it might increase or decrease depending on the number and attitude of the audience.
  A small, friendly gathering is much more open to manipulation than a large, hostile crowd.
  The number of successes indicates the strength of the orator's hold on the audience (if she has no successes, her influence is negligible).
  A botched oration roll may result in damage to the character's reputation or a friendly crowd becoming hostile.
  Proper preparation (Intelligence + Expression) might increase or decrease the difficulty of the oration.
  Spontaneity (real or apparent) makes the speaker appear more genuine, but a well-written speech can make the difference between success and failure.
\fi

\paragraph{Performance [Charisma + Performance]:}
Storytelling, musical recitals and plays involve one or more entertainers performing before an audience.
When a character is involved in such entertainments roll Charisma + Performance.
The base difficulty is 6, but it might increase or decrease depending on the audience's mood and the material used.
A hostile audience is hard to please, while the quality of a well-written play (see ``Artistic Creation'') shines through even a poor performance.
One success indicates that the performance goes well but is uninspiring.
Three successes result in a memorable event that is the talk of the court for weeks.
Five or more successes is a truly magnificent performance that is remembered for years.
Failure indicates one or more minor problems, while a botch is a truly dreadful performance, one that is remembered for all the wrong reasons.


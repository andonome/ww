\chapter{Bestiary}
\label{bestiary}
\index{Bestiary}
\index{Animals}

\begin{multicols}{2}

\ifDA
  Animals are an inescapable part of medieval life.
  The vast majority of the population lives off the land, works fields with the help of beasts of burden and raises fowl, pigs and goats for meat, eggs and cheese.
  Wild beasts, from boars and deer to bears and wolves, roam the unending forests.
  Even in the cities, animals are omnipresent, not only in the form of rats and carrion birds, but in the dogs, pigs, goats and fowl that are part of households of any size.
  And of course, the medieval knight derives much of his status from his steed.
  The following bestiary details the more common beasts along with some of the arcane things that haunt the night.
\fi

\ifvamp
  Vampires are creatures of the cities and rarely interact with the beasts of the wild.
  Indeed, most animals fear the Children of Caine, hissing or snarling as the unnatural creatures approach.
  Nonetheless, certain vampires, particularly those with the
  Animalism Discipline, employ animals as companions, spies or soldiers.
  Then, too, animals are occasionally changed into ghouls, particularly by the vampires of Clans Gangrel and Nosferatu.
\fi

\subsubsection{Animal Traits}
\index{Animals}

\paragraph{Attributes:}
Natural animals have five Attributes instead of nine.
Their Physical Attributes (Strength, Dexterity, Stamina) function as with mortals, although animals may not be capable of some feats of strength (see p. \pageref{featsOfStrength}) because of their inability to lift things.
Animals have only two Mental Attributes: Perception and Wits.
Perception is essentially the same as with mortal humans, as an aggregate of their various senses.
Most animals have a high Perception, which can reach levels higher than 5.
Wits is a basic measure of the animal's instincts and survival skills.
In rough terms, it measures a prey animal's ability to escape predators and a predator's ability to catch prey.

\paragraph{Willpower:}
Willpower is essentially the same for animals as it is for people: a measure of mental fortitude and resistance to outside influences.
Animals cannot spend points of Willpower.

\paragraph{Health Levels:}
Depending on their size, animals have a varying number of health levels.
These are expressed in terms of each level's dice-pool penalty.
Animals whose health levels include
Incapacitated can survive longer than those without it.
Others die when they run out of health levels.

\ifvamp
  The Blood Pool Trait reflects how many points a feeding Cainite can drain from a beast.
  Note that animal blood is far less satisfying than human vitae; some animals that have more blood than a human actually have lower blood pool ratings.
\fi

\paragraph{Attacks:}
Most animals have some form of natural weapon or form of attack, from a horse's kick to a wolf's bite and claws.
Biting, clawing and goring generally causes lethal damage.
Trampling and kicking causes bashing damage.

\paragraph{Abilities:}
Animals have ratings in Abilities to represent their natural proficiencies in such things as tracking (Alertness and Survival), using their natural attacks (Brawl) and escaping harm (Athletics).
Abilities listed in brackets are those typically resulting from training at the hands of humans. (Use common sense in applying these Abilities: Regardless of its Athletics rating, a cat cannot throw a spear!)

\ifDA
  \section{Beasts of Home and Farm}

  \begin{bigTable}[@{}llllX@{}]{Horse/Mount Table}

  Mount & Trample Dam. & Move (y/turn) & Notes \\

  Riding Horse	& 	6B	&	15	&  \\
  Arab Horse	&	5B	&	20	&  \\
  Warhorse	&	7B	&	12	& Available in Iberia or Saracen lands only. \\
  Camel		&	5B	&	10	& Available in Saracen lands only, -1 difficulty to avoid being trampled. \\
  Elephant	&	9B	&	10	& -2 difficulty to avoid being tramples \\

  \end{bigTable}
\else
  \animal{Alligator}%
  {{4}%%% Body
  {2}%
  {5}%
  }%
  {%%% Mind
  {4}% Perception
  {4}% Wits
  {3}}% Willpower
  {
  Alertness 2, Athletics 2, Brawl 2, Stealth 3

  \textbf{Attack:} Bite for seven dice; tail slap for six dice

  \textbf{Blood Pool:} 5
  }% Abilities

\fi

\animal{Cat}%
{{1}%%% Body
{2}%
{3}%
}%
{%%% Mind
{1}% Perception
{3}% Wits
{8}}% Willpower
{Alertness 3, Athletics 2, Brawl 2, Climbing 3, Stealth 4, Empathy 2, Subterfuge 2}% Abilities

\animal{Cow/Ox}%
{{3/5}% Body
{2}%
{3/5}%
}%
{{2}% Mind
{1}
{3}}%
{
Alertness 2, Brawl 0/3
}% Abilities

\animal{Dog/Hound}%
{{4}%%% Body
{3}%
{3}%
}%
{%%% Mind
{6}% Perception
{3}% Wits
{2}}% Willpower
{
Alertness 3, Athletics 2, Brawl 3 (Empathy 2, Stealth 2)
}% Abilities

\animal{Horse}%
{{4}%%% Body
{2}%
{3}%
}%
{%%% Mind
{3}% Perception
{3}% Wits
{2}}% Willpower
{
Alertness 3, Athletics 2, Brawl 1
}% Abilities

\ifDA
  \animal{Warhorse}%
  {{6}%%% Body
  {2}%
  {5}%
  }%
  {%%% Mind
  {3}% Perception
  {3}% Wits
  {4}}% Willpower
  {
  Alertness 3, Athletics 2, Brawl 3, Empathy 2}% Abilities
\fi

\animal{Mule}%
{{4}%%% Body
{2}%
{3}%
}%
{%%% Mind
{1}% Perception
{1}% Wits
{6}}% Willpower
{
Alertness 2, Brawl 2
}% Abilities

\animal{Pig/Boar}%
{{2}%%% Body
{2}%
{4}%
}%
{%%% Mind
{3}% Perception
{3}% Wits
{3}}% Willpower
{
Alertness 2, Athletics 2, Brawl 2
}% Abilities

\animal{Rat}%
{{1}%%% Body
{3}%
{1}%
}%
{%%% Mind
{5}% Perception
{4}% Wits
{4}}% Willpower
{
Alertness 2, Brawl 1, Stealth 3
}% Abilities

\animal{Sheep}%
{{2}%%% Body
{2}%
{2}%
}%
{%%% Mind
{1}% Perception
{1}% Wits
{2}}% Willpower
{
Alertness 2, Empathy 2
}% Abilities

\subsection{Beasts of Wild and Wood}

\animal{Bat}%
{{1}%%% Body
{3}%
{2}%
}%
{%%% Mind
{6}% Perception
{2}% Wits
{2}}% Willpower
{
Alertness 3, Stealth 2
}% Abilities

\animal{Bear}%
{{5}%%% Body
{2}%
{5}%
}%
{%%% Mind
{3}% Perception
{3}% Wits
{4}}% Willpower
{
Alertness 3, Brawl 3, Stealth 1
}% Abilities

\ifDA\else

  \animal{Big Cat}%
  {{4}%%% Body
  {3}%
  {4}%
  }%
  {%%% Mind
  {4}% Perception
  {5}% Wits
  {2}}% Willpower
  {
  Alertness 3, Athletics 2, Brawl 3, Stealth 3
  
  \textbf{Attack:} Claw for four/five dice; bite for five/six dice

  \textbf{Blood Pool:} 5
  }% Abilities

\fi

\animal{Bird of Prey}%
{{2}%%% Body
{3}%
{3}%
}%
{%%% Mind
{7}% Perception
{4}% Wits
{3}}% Willpower
{Alertness 3, Athletics 2, Brawl 1, Brawl 3}% Abilities

Notes: This template can represent a hawk, crow, raven, owl or even vulture.
A bird can typically fly at 25 to 50 mph.

\animal{Deer/Stag}%
{{1}%%% Body
{3}%
{2}%
}%
{%%% Mind
{4}% Perception
{2}% Wits
{3}}% Willpower
{
Alertness 2, Brawl 2, Empathy 2, Stealth 2
}% Abilities

\animal{Hare}%
{{1}%%% Body
{3}%
{2}%
}%
{%%% Mind
{5}% Perception
{5}% Wits
{1}}% Willpower
{
Alertness 2, Empathy 1, Stealth 4
}% Abilities

\animal{Wolf}%
{{3}%%% Body
{3}%
{3}%
}%
{%%% Mind
{6}% Perception
{4}% Wits
{4}}% Willpower
{
Alertness 2, Athletics 1, Brawl 3, Stealth 2
}% Abilities

\begin{bigTable}[XXXX]{Pack and Swarm Attributes}

\textbf{Animal} & \textbf{Damage} & \textbf{Health Levels} & \textbf{Initiative} \\

Small bugs                    	& 1 & 5 	& 2 \\
Large bugs                    	& 2 & 7 	& 3 \\
Flying bugs                   	& 2 & 5 	& 4 \\
Birds, bats                   	& 4 & 9 	& 5 \\
Rats  (one or more feet long)   & 3 & 7 	& 3 \\
Large rats                      & 4 & 9 	& 3 \\
Feral cats                      & 4 & 6 	& 6 \\
Wild dogs                       & 6 & 15	& 4 \\

\end{bigTable}


\section{Packs and Swarms}

Although the traits listed here detail individual creatures, some animals attack en masse.
Also, it's quite atmospheric for a vampire or demon to overwhelm its victims with rodent or canine minions.
If a swarm of hornets or horde of rats accosts the characters, use the rules that follow.

Instead of trying to determine what each and every member of a pack or swarm does, simply roll to see if the swarm itself harms a character.
Narrate the results from there.

Each beast type is given a listing on the following chart.
Roll the damage dice pool listed once per turn (difficulty 6) and allow the characters to try to dodge or soak the result.
This damage is lethal, or possibly bashing in the case of small or weak creatures.
Packs attack once per turn per target and act on the initiative given on the chart.

If a character dodges, he can move normally for the remainder of the turn.
Otherwise, his attackers slow him down to half his usual movement.
If they score more than three health levels worth of damage in one turn (after the target soaks), or if the player botches an appropriate roll, the character is knocked down and overrun.
He can move only one or two yards per turn, and the swarm's damage difficulty falls to 5.
Efforts to get back up and continue moving have higher than normal difficulties (typically difficulty 7 or 8).

The health levels listed reflect the amount of damage it takes to disperse a pack or swarm.
An additional two health levels destroy the attackers completely.
Arrows and small mêlée weapons
(knives, claws, bare hands) inflict a single health level per strike, no matter how many attack or damage successes are rolled (that is, the strike hits only one creature).
Large mêlée weapons
(swords, staves, boards) do normal damage (each damage success rolled eliminates one health level of the swarm as a whole), as do large-area attacks (Greek fire, frost storms, gusts of wind).
Swarms and packs don't soak.

Depending on the size of the pack, two or more characters might be affected by it and can attack it in return.
Anyone who helps an overrun character can be attacked as well.
A human can outrun some packs or swarms (those consisting of rats or bugs), but can't hope to outrun others (those consisting of hyenas or birds).

\ifvamp\ifDA
    \index{Zombie}\index{Athanatos}\index{Mortis!|see{Athanatos}}
    \section{Created Creatures}
    \label{zombie}

    \character{Athanatos Corpse Knight}%
    {%%% Body
    {3}% Strength
    {3}% Dexterity
    {4}}% Stamina
    {%%% Mind
    {0}% Charisma
    {0}% Manipulation
    {0}% Appearance
    {1}% Perception
    {1}% Intelligence
    {2}% Wits
    {0}}% Willpower
    {Archery 2, Athletics 2, Brawling 2, Mêlée 3}% Ability
    {{}}%
    {{Sword, composite armour}}% Equipment
    {{}}% Backgrounds

    Athanatos (Greek for ``without death'') are terrible warriors raised from the grave by the \nameref{cadaverousAnimation} path (page \pageref{cadaverousAnimation}).
    Assuming the Cappadocian maintains control of the knight, it obeys her.
    \textit{Athanatoi} move as normal humans.
    As trudly dead things they are immune to most effects of mind-altering Disciplines, and they soak lethal and bashing damage like vampires.

    \character{Corpse Servant}%
    {%%% Body
    {2}% Strength
    {2}% Dexterity
    {3}}% Stamina
    {%%% Mind
    {0}% Charisma
    {0}% Manipulation
    {0}% Appearance
    {2}% Perception
    {1}% Intelligence
    {1}% Wits
    {0}}% Willpower
    {Crafts 1, Seneschal 2}% Ability
    {{}}%
    {{}}% Equipment
    {{}}% Backgrounds

    Corpse servants are shambling bodies given a semblance of life by \nameref{cadaverousAnimation} path.
    They serve as assistants and obey their master.
    They cannot speak, and they move slowly, shambling at five yards per turn.
    As truly dead things, corpse servants are immune to most effects of mind-altering Disciplines, and they soak lethal and bashing damage like vampires.
  \fi
\fi

\ifDA
  \input{spirits.tex}
\fi

\end{multicols}

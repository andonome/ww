# White Wolf in LaTeX

This is a recreation of White Wolf's rules, made with LaTeX.

## The Golden Rule

> The most important rule is simple, but all-encompassing: Above all, have fun.

> This means that if the rules or systems in this book interfere with your enjoyment of the game, feel free to change them. The world is far too big to be reflected accurately in any set of inflexible rules. Think of this book as a collection of guidelines, suggested but not mandatory ways of capturing the Dark Medieval in the format of a game. You're the arbiter of what works best in your game, and you're free to use, alter, abuse or ignore these rules at your leisure.

## Changes

The combat system has gone the way of the White Rhinoceros; instead the rules use a generic contest system which gives penalties to any opponent, whether one is investigating someone's crime, stabbing someone with a rusty knife, or vying for attention.

The original rules sit in another branch, in case anyone wants to change them.

## Structure

LaTeX allows pdfs to receive options.
You can compile this with the `Vampire` or `DA` ('Dark Ages'), which add the appropriate styles, examples, et c.

You can use the [style-sheet](https://gitlab.com/andonome/wwtex) submodule on its own to stylize another book.

|             | **Dark Ages**                          | **Modern**                |
|:-----------:|:--------------------------------------:|:-------------------------:|
| **Basic**   | [Dark Ages][Dark Ages]                 | [Modern WoD][WoD]         |
| **Vampire** | [Dark Ages Vampire][Dark Ages Vampire] | [Modern Vampire][Vampire] |


# Requirements

This works on Arch and Void Linux.
It probably works on other Linuces.

Download:

> git clone https://gitlab.com/andonome/ww

Dependencies:

- git-lfs
- `texlive-full`
- `inkscape` (version 1.2.1 or later)
- `make`

In general, the latest version is used for everything.

To make the document:

> make

[Vampire]: https://gitlab.com/andonome/ww/-/jobs/artifacts/master/raw/Vampire.pdf?job=compile_pdf
[Dark Ages]: https://gitlab.com/andonome/ww/-/jobs/artifacts/master/raw/Dark_Ages.pdf?job=compile_pdf
[Dark Ages Vampire]: https://gitlab.com/andonome/ww/-/jobs/artifacts/master/raw/Dark_Ages_Vampire.pdf?job=compile_pdf
[WoD]: https://gitlab.com/andonome/ww/-/jobs/artifacts/master/raw/World_of_Darkness.pdf?job=compile_pdf
[wiki]: https://gitlab.com/andonome/ww/-/wikis/changes

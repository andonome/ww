SOURCES = $(wildcard *.tex cc drama intro storytelling vampire wwtex)

include wwtex/vars
wwtex/vars:
	git submodule update --init

MAIN_TEX_FILES != grep -lF '{wwtex/WoD}' *.tex

OUTPUT = $(patsubst %.tex,%.pdf,$(MAIN_TEX_FILES))

.PHONY: out
out: $(OUTPUT) ## Kiss a vampire

$(OUTPUT): $(MAIN_TEX_FILES)

clean:
	$(RM) *.pdf
	cd wwtex && make clean

\chapter{Storytelling}
\label{storytelling}
\index{Storytelling}
\begin{multicols}{2}

\ifvamp
  Forget about the pages of rules and the handfuls of dice.
  Close the book, turn out the lights, and tell me a story about dark desires and relentless hunger.
  I'll tell you about a vampire, about her talents and her weaknesses, and you tell me what kind of challenges she faces, what rewards or perils come her way.
  You plan the twists and turns the story will take, and I will tell you how the vampire navigates them.
  Only you know how the story ultimately ends, but only I know how the vampire will arrive there.
  Along the way, the work you put into the story gives my vampire the chance to grow and develop, and her actions breathe life into the world you have created.
  
  That is the challenge of storytelling.
  \longSetting\ is about the inner struggle between humanity and monstrosity in the face of unfettered power and eternal life.
  No mortal law binds the Kindred, no moral code restrains them -- only fading passions or ideals nurtured in mortal life keep a vampire from indulging her horrific nature, and those memories become harder and harder to recall as the years stretch into centuries.
  In short, \setting\ is about the characters and how they develop -- or wither -- in the face of tragedy and temptation.
  Can a mortal steeped in religious faith reconcile her deeply held beliefs with her lust for blood? Can a vampire resist the temptation to Embrace her lover rather than face an eternity of loneliness?
  The Beast awaits any Kindred who surrenders herself completely to her predatory urges.
  The Storyteller must draw on the characters' backgrounds, hopes, and ambitions to create stories that challenge their -- and their players' -- convictions and beliefs.
  As a result, taking on the role of a Storyteller in a Vampire game is very demanding, requiring careful thought and background work to build character-driven chronicles and stories.
  You must create a world that is a nightmare reflection of our own, enticing and repellent, exhilarating and horrifying.
  You must evoke the thrill of inhuman power and a fear of what might happen if the character ever loses control.
  And you can't let the characters just keep to themselves and survive off daring thefts from the local blood bank.
  The night air is thick with the intrigues of the elders as Gehenna draws nigh, and the ancillae manipulate neonates as they see fit, promising great rewards -- and even greater risks.
\else
  Remember the last time you saw a really great movie? Remember how it felt? When we watch a film we really enjoy, time seems to stand still.
  The rest of the world is set aside, and the experience consumes our senses.
  From the opening scene to the end credits, that film becomes our world.
  Afterwards, we can say that it's ``just a movie,'' but then again, part of us knows that isn't exactly true.
  
  A movie is a way of telling a story.
  If it's a really good film, we find a reason to get caught up in it.
  Maybe it's a character with whom we can identify or a scene that makes us feel something.
  We watch hoping to find something we like.
  The motion-picture medium has one big drawback, however -- it's a passive experience.
  A person in the audience can choose whether he'll have butter on his popcorn or where he wants to sit in the theater, but that's about it.
  He will have his own reasons for liking the film, but someone else has put the meaning into it.
  
  A lot of stories are like that.
  With books or plays, television programs or conics, the end result is the same.
  Someone else tells the story, we watch it or read it, and take what we're given.
  The best we can hope for is to read something into it that wasn't originally there.
  
  Storytelling games are another medium for telling stories, a medium just as valid as movies or television.
  Each entertainment has its own strengths and weaknesses, but storytelling has an overriding strength -- it's a personal experience.
  The people involved tailor it to what they want it to be and rework it for themselves.
  
  With the assistance of a group of players and the advice in this book, each of you will bring your owns story to life.
  You're not going to perform for an audience of millions, nor will you be dependent upon a budget of millions.
  With a few dice, some friends and maybe a little pizza, you'll lead your troupe of players on a journey that no movie or television program can ever fully reproduce.
  A game is just a game, but a well-told story is a work of art.
 
\fi

Storytelling sounds like a lot to manage all at once, and it is at first.
Fortunately, the Storyteller doesn't have to do it all herself.
The secret to successful Storytelling is, ironically, the work of the players.
Fulfilling the expectations and interests of a chronicle's players is the first trick to creating the game's setting.
Then -- if the chronicle and its overall story have been carefully developed -- the actions of the characters, both good and bad, will have consequences that in turn spawn further stories.
Never forget: The more the players are involved with what happens in a chronicle, the less work you, the Storyteller, must take upon yourself.
You aren't supposed to do it all alone.
The Storyteller should have as much fun with the game as the players, and this chapter details how.

This chapter illustrates the process of creating and running a \setting\ chronicle, and offers advice for making the most out of the individual stories that carry the chronicle along.
Building a detailed and cohesive background, a world for your players to hunt in, begins with input from your players and your own ideas for what kind of an overall story you would like to tell.
Once you have decided on the details of the setting, the next step is creating characters to fill it, again keeping in mind the kind of chronicle you want to tell.
After the characters are in place, you can then get to writing the chronicle in earnest, working out the intrigues and events that move the overall story along and draw on each character's goals and motivations.
Each step builds upon the next, giving you more and more background to make each story enjoyable and easy to manage.
If you have never run a roleplaying game before, don't be intimidated by the big picture.
Take it a step at a time, do it for fun, and let your imagination run wild.

\ifvamp
\subsubsection{The First, Fleeting Glimpse}

  It starts with a couple of ideas.
  You read through the book, and some things jump out at you.
  Maybe it's the image of a haughty prince ruling a city with an oppressive hand, or perhaps you like the idea of a band of \ifDA gangrel\else anarchs\fi making their own rules and living like wolves in the urban wilderness.
  Something catches your eye and sets off a spark in your mind, and you think, ``This could make a cool chronicle''.
  The question is, how do you turn these nebulous ideas into a well-rounded foundation for the stories you want to tell?
  
  The first step begins with the players.
  Before you can really develop the foundation for your chronicle, you must have a strong grasp on what sorts of characters they want to play and how their concepts relate to your ideas.
  Suppose you are considering a chronicle set against the backdrop of a prince's meteoric rise and fall in a large, important city.
  You might envision stories of intrigue, treachery and the corrupting influence of great power.
  But what if one player has her heart set on playing a Nosferatu\ifDA\else anarch\fi, and another wishes to be an apolitical, narcissistic Toreador, neither of which is compatible with your overall concept?
  It's never a good idea to force a character concept on a player, because you want players to feel like they are contributing to the game and playing characters in whom they are really interested.
  At this point comes some amount of negotiation and compromise; perhaps you can interest them in your backdrop of political conflicts and double-dealing, but shift the focus of the chronicle to center instead on the characters' struggles to avoid the plots of their elders while pursuing their individual agendas.
  The important point is to make sure that your ideas and the players' expectations are in synch before you even begin to develop the chronicle.
  This way the players can add their ideas to your own and make your job much easier in the long run.
  
  Once everyone agrees on the general idea of the chronicle, the players can begin to create their characters and you can begin to create the world in which they will hunt.
  It's worthwhile to do this simultaneously because it allows your ideas and theirs to play off one another, and might point you in directions that you could have missed otherwise.
  \ifDA\else
  Suppose, for example, that a player wants her character to have been a government agent prior to her Embrace.
  You could then take this idea and expand upon it: The character worked for a covert division within the FBI, performing counter-intelligence work that specifically investigates high-level corporate executives and politicians.
  This division has in fact been subverted by a powerful vampire who uses it as a resource to gather influential information and hinder the activities of her rivals.
  To add yet another level of conflict, you might decide that this primogen is now eyeing another member of the division for the Embrace, someone whom the character had a friendship with (or perhaps loved) as a mortal.
  You can then encourage the player to further develop the agency her character worked for, letting her provide details such as a history and important personalities that you blend into your world.
  This lets her flesh out her character, allows her to contribute to the game as a whole, and gives you valuable resource material for your chronicle.
  Get your players to go into a lot of detail when creating their characters.
  Sit in with them during the creation process, and brainstorm about their backgrounds, then make the most of the information they come up with.
  They can provide you with a whole host of characters, situations and conflicts that will be of use to you later.
  \fi
  
  It is important to take the characters' Backgrounds into account and develop them in detail, because as neonates these newly
  Embraced vampires still have very strong ties to the lives they have left behind.
  Encourage the purchase of allies, contacts, influence and the like, then brainstorm with the player to flesh them out.
  Where do a character's resources come from? Is she an heiress?
  \ifDA
    Is she literate?
    Did she grow up in the squalor or a city, or near a wild forest?
  \else
  Did she win the lottery?
  Did she stumble onto a drug deal gone sour and steal the bloodstained cash?
  What effect do these circumstances have on who the character is and her place in the chronicle?
  \fi
  Likewise, allies or contacts are more than just dots on the character sheet -- they are people with their own feelings and emotions.
  For instance, suppose a player wants her character to have a mid-level contact in the \ifDA priesthood\else police department\fi.
  Who is this contact, and how did the character establish this relationship?
  The contact might be the character's uncle, \ifDA a soldier just back from the crusades\else a veteran homicide detective who has a habit of asking pointed questions about the character's lifestyle and activities (particularly if he catches her at the scene of a recent murder!)\fi.
  
  Each Background is an added dimension to the character concept, containing a wealth of ideas to inspire a Storyteller.
  How do the characters handle the sudden and irrevocable separation from everything they have ever known or loved?
  Do they fake their deaths, or simply walk away from their mortal lives?
  Do the characters leave loved ones who simply will not accept their disappearance and go to any lengths to find them?
  Can the characters stay away from spouses or children, torn by love yet knowing what might happen one night when the Hunger overtakes them?
  These situations are some of the first dilemmas that the characters must face, and can influence their actions in many subtle ways.
  While it is always tempting to just gloss over the particulars of each character's Embrace and get on with ``being a vampire,'' this leaves out a vital dimension in the character's struggle to maintain her humanity, and provides you with a fertile field to draw ideas and supporting characters.
  
  Once you have determined a general direction for your chronicle and incorporated elements of the players' characters, you can make some decisions about the world in which their stories will take place.
  Having detailed the setting for the chronicle (usually a city, but it is possible to run small-town or even wilderness chronicles) and peopled it with supporting characters that add to your overall concept, you can begin shaping the chronicle in earnest.

\fi

\subsection{A World Dark and Deadly}

Before your chronicle can be written, there must be a stage where its actions can be played out.
You need to create a setting for your stories, a world that supports the themes and ideas you want to explore in your chronicle and starkly illustrates the glories and terrors of undead existence.
\ifvamp
  The world of the vampire is dark, dangerous, enigmatic and rich in imagery.
\fi
Consider these guidelines when inventing the details of your world:


\paragraph{The Extinction of Virtue:}
\ifDA\else
  There are few illusions left in the World of Darkness.
  Centuries of greed and deceit \ifvamp (on the part of humans as well as Kindred)\fi have eroded humanity's innocence.
  \ifDA
  Witchcraft and fornication permeate everything, from the nobility to the priesthood.
  The pope only cares for power, not saving souls.
  \else
    Cynicism and despair permeate everything, from the tags on city walls to the movies in theaters.
    No one dares to believe in much of anything, because virtues like compassion and charity are just invitations to be victimized.
  \fi
  A gentle soul and a loving heart are rare as diamonds, and as precious.
\fi

\paragraph{Blood and Money:}
Life is cheap, and desperate people resort to violence out of frustration, fear, hatred and greed.
\ifDA
  People duel to the death over petty squabbles, and the victim's family can only request the money their son would have made.
  Women are sold off to service, or married away to get rid of them.
  Every ten years a new war is announced so that some distant warlord expand his territory.
\else
  Crime is ever-present, and many families and neighborhoods adapt a siege mentality against the rest of the world.
\fi
It's us or them.

\paragraph{No More Good Guys:}
The world has lost its heroes.
They were caught in sex scandals or taking bribes, or perhaps they fell victim to endemic \ifDA city-wide plagues\else urban violence\fi.
There is no strong leadership, no faith in \ifDA the nobility\else politicians\fi or belief in building a better tomorrow.
People know better.

\ifDA\else
  \paragraph{Haunted Houses:}
  Humanity is rotting from within, continuing a sad, slow decline, and symbols of its decay are everywhere in the weathered facades of great, Gothic churches and granite office buildings.
  Amid soulless towers of steel and glass might sit an abandoned cathedral whose stained glass is rich with color and beauty from a time now lost.
  Such a place serves as a haunting reminder of what might have been, or could be again.
  
  \ifvamp
  \paragraph{The Rage of the Millennium:}
  The end of an age draws nigh, and already the rumors and prophecies fly.
  Will it be a new beginning, or the end of the world? Many Kindred and mortals alike fear for what is to come and preach of apocalyptic doom, while others take to the streets with savage, desperate abandon, determined to make their mark on the world before it all goes up in flames.
  \fi
\fi

These points illustrate the essence of \setting, and they are important because they heighten the dilemmas that your characters face as they grapple with their fading humanity.
Despair and resignation are all around them; violence and death are common.
What is one more killing, one more lie?
How much difference can one person\ifvamp, even a vampire, \fi make?
Virtues like courage and compassion are hard to find and even harder to maintain, but it is the struggle for them that is important.
This struggle is the source of the game's triumph and tragedy, and the decisions you make in developing your setting should take this into account.
It is important to point out that you don't have to adhere religiously to these concepts, and the degree to which you emphasize them is strictly a matter of personal taste.
The only truly important thing to remember is that your environment should be one in which doing the right and honorable thing is difficult and daunting.

There is, of course, no limit to the possible physical locations you can choose for your chronicle -- with enough imagination and forethought, a \setting\ chronicle can be set anywhere from \ifDA Belgrade to Svalbard\else Washington, DC to the Amazon Basin\fi.
\ifvamp
  The best locations, of course, are major cities, because they allow for a large population of vampires and are focal points for the money and power that most Kindred seek.
  If you wish to locate your chronicle in a large city that you are unfamiliar with, your local library can provide useful information and maps, which you can then reinterpret to suit your purposes.
\fi
Remember, though, that you aren't constrained to be faithful in every detail; this is the World of Darkness, and you can shape it any way you choose.
Consider the guidelines above, and where necessary alter the details in favor of your own ideas, or to build the proper mood.
Many Storytellers prefer to set their chronicles in dark reflections of their own hometowns, which allows them and their players to draw upon everyday knowledge to help envision the places that their characters visit.

As you map out the length and breadth of your locale, draw ideas from important city features, combining function with symbolism to make interesting images.
For instance, an abandoned and decaying train station in the center of the city might make an ideal site for Elysium, with its high, vaulted halls and faded grandeur.
\ifvamp
  A little-used cathedral might become a haven for the city's Caitiff, who are drawn to its symbolism of sanctuary and redemption.
  A half-built zoo could serve as the anarchs' playground, or a block of skeletal construction sites could give the Nosferatu an aerie to look out over the streets.
  Vampires are territorial creatures, and the places they claim invariably mirror their individual character and attitudes.
  Again, don't let hard reality dissuade you from going with a cool idea; if you want your prince to rule from a Victorian mansion but your city doesn't have one, make one and put it where you want.
  In \setting\, details always take a back seat to mood and imagery.
\fi

So at this point you have three sources to draw on to build the setting of your chronicle: You have your own general ideas, you have all the details provided by your players during character creation, and you have at least some knowledge of the locale you have chosen.
Now comes the time to develop your chronicle in detail.

\subsection{Written in Blood}

The chronicle is the overall story that the Storyteller wishes to tell.
It is made up of a series of smaller stories in which the player characters are the central figures.
Think of a chronicle as a collection of books that tell a long, complicated tale.
Each book is a story unto itself, which is further broken up into chapters, and then into scenes.
What happens in each individual story depends a lot on the course of the chronicle as a whole.
It's this stage of development that is the most demanding and time-consuming for the Storyteller.
Unlike many other open-ended RPG ``campaigns,'' \setting\ chronicles have a definite beginning, middle and end.
Accordingly, you need to detail this structure in advance, in order to organize your thoughts, show you when to pick up the pace, and provide tension over the course of individual stories.
A chronicle loses its focus and energy if there is no real end in sight.
After all this work, you want to close things off with a bang, not a whimper, right?
Get a journal or Nextcloud account and set it aside to hold notes and ideas as you outline the course of events in your chronicle.
Don't try to keep it all in your head.

At this point, you have a pretty large amount of information to help guide your development of the chronicle; now you have to flesh out the course the stories will take and blend all of the details into a workable whole.
The first step is to choose a governing theme.
A theme is the central idea that describes the basic plotline of the overall story.

\ifvamp
  \input{vampire/9-storytelling.tex}
\fi
\input{storytelling/sq.tex}

\section{The Commandments}

The art of storytelling is a process, like any artistic endeavor, and at first it seems like an overwhelming task.
The main elements to remember, though, can be broken down into five ``musts'' and five ``must-nots'':

\paragraph{Involve the Players Whenever Possible:}
Incorporate their ideas and backgrounds into your city and chronicle.
This will take some of the burden of world-building off your shoulders and give the players more of a stake in the story you are telling.
Ultimately, the players should be the most important -- though not necessarily the most powerful -- denizens of your chronicle.

\paragraph{Accommodate the Players' Expectations:}
It's their game too, remember.
You need to have some idea of what kind of game the players want to play before developing your chronicle.

\paragraph{Work Things Out in Advance:}
The more you have worked out before game time, the more attention you will be able to devote to telling the story.
If you've taken the time to think through the story's various twists and turns, you will be better able to roll with the inevitable player curve ball.

\paragraph{Story First, Rules Second:}
Do not let the tale you want to tell get held back by the rules.
You can make them or break them as you see fit, if doing so enhances the story and makes it more enjoyable for the players.

\paragraph{Description, Dialogue, and Action:}
Make your world come alive with vibrant description, involving sights, smells, taste and touch.
Encourage roleplaying by acting out conversations, using different voices to individualize your characters.
Keep the pace and intensity high with dynamic action.

Conversely\ldots 

\paragraph{Avoid Stereotypes:}
Nothing drains the life out of your chronicle faster than an endless parade of identical, cardboard characters.

\paragraph{Don't Forget the Payoff:}
If the players work hard and make smart decisions, their characters' success must be in proportion to the challenges they have faced, or they will feel cheated.

\paragraph{Don't Tell Them Everything:}
Much of the challenge in a game is in the mystery, the parts of the story that you hold back for the players and their characters to discover on their own.

\paragraph{Don't Abuse Your Power:}
You are the final arbiter of events.
Your word is law, but you cannot use this authority to beat the characters into doing what you want them to do.
It is a game, it's for fun, and everyone should have a good time, whether it follows the script or not.

\paragraph{Don't Panic:}
If the players pull the rug out from under you, don't be afraid to call a break and take some time to collect your thoughts.
It will happen a lot at first, but after a while you will be able to handle anything they throw at you.

\end{multicols}

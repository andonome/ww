\index{Medieval!Setting}
\section{A World Darkly Lit}

``Dark Medieval'' is the phrase we use to describe the world of \longSetting.
It is a world in which vampires and werewolves prowl the night, and the powers of magic and faith bring the spark of the supernatural into the lives of everyone, from the nobility to the peasants tilling the fields.
Priests still bless the fields, and village wise women are consulted for every worry, from matchmaking and fertility to curing wars.
Many folk still pay homage to the ``Fair Folk,'' or to older pagan gods.
But this is not a J.R.R.  Tolkien fantasy land -- elves, dwarves and the like are mere children's stories, and the few who claim to have seen such mysterious beings in the deepest forests are dismissed as madmen.
Plagues threaten entire cities with the spectre of death, and while the Church wields tremendous temporal power, only the most devout saints can perform miracles.

The so-called Dark Ages of our world was the period between the fall of the Roman Empire and the beginning of the Renaissance.
The glory that was Rome crumbled, and the roads and bureaucracy the Empire had brought to the continent quickly fell apart.
Trade decreased, and most people were too busy simply trying to survive to spend time learning to read and write.
Knowledge of science and technology common during Roman occupation was forgotten, and a cloud of superstition descended on the Western world.

Times were difficult for the common man during this period.
Marauding armies sacked towns and pillaged villages.
Cities lay in ruins.
Most of the art produced during this time was portable and often practical, such as jewelry and pottery.
Even some rulers were illiterate, and only in scattered Christian monasteries did reading, writing and academic learning continue.
Most people were farmers, never blessed with the luxuries of art or learning, living hard, short lives, more at risk from disease and starvation than from invasions.

But the ``dark'' part of Dark Medieval means something more, and is not restricted to a single decade or century.
The shadow that has fallen across much of Europe is a moral darkness, a rejection of the spiritual.
Respect for one's fellow man has been eclipsed by the day-to-day struggle to survive.
Many commoners obey the laws of God and man more out of fear of the consequences than from any real belief in what is ``good'' or ``right''.

\ifvamp
  The vampires who hold much of the power rule unchecked, and rarely face the consequences of their actions.
  But the mortal world will notlive in fear forever, and the day may come when the living will rise up against the predators in their midst.
\fi

In the end, you will decide what the Dark Medieval world is like in your chronicle.
It may not be as dark as we've portrayed it, or it may be a living Hell on earth.
It can be historical or fantastic, a world of violence or intrigue, and will probably be all of these things at one point or another.
Use the tools in this book with your own imagination to create a world that is uniquely your own.

\subsection{Dark Reflections}

The Dark Medieval thus blends history and horror.
It is a doomsday time when parish priests warn that the signs of the Apocalypse are everywhere and that devils tempt the pious and saintly.
Villages and hamlets exist in total isolation, surrounded by deep woodlands filled with the ghastly howls of man-beasts and the chants of witches practicing their damned arts.
Across Europe, fortified cities have bloomed with the false promise of safety and freedom.
In truth, their crooked cobblestone streets and labyrinthine alleys are virtual prisons from which not even the dead can escape.

The Dark Medieval is flavored with isolation and desolation.
Vast miles of untamed hinterland separate villages from cities and hide things that are much worse than monsters.
Forests are forbidding expanses, shielded by shadows during the day and illuminated by the faint light of the moon at night.
The wind howls through castles, lone sentinels whose walls have been stained red by blood.
Superstition and fear keep villages and nobles alike confined to their homes, cowering around feeble candles until morning.
In universities and monasteries, scholars and scribes struggle to pierce this uncertain darkness with ancient fragments and half-remembered truths.
Kings and counts wage desperate war to keep the darkness at bay, the Church tries to spread the light of Christianity through misguided crusades, and Cainites rule the night.

\subsection{By Day}

To the people of Europe, it is Anno Domini 1230 -- the 13th Christian century is reaching its midway point.
Although it is a time of relative quiet among mortal rulers, the 13th century is still one of strife and unrest.
The century started with the bloody and mismanaged Fourth Crusade
(1202-1204), redirected by Venetian interests into sacking and destroying Constantinople, the greatest of Christian cities.
In England, King John Lackland (1199-1216) signs the Magna Carta shortly before his death, establishing the duties and responsibilities of the king to his subjects.
His heir, Henry III (1216-1272), finds his power challenged throughout his troubled reign by the powerful English barons, making him a weak king and drawing his attention away from France.
Using this to his advantage, Philip II Augustus of France (1180-1223) reclaims most of the territories in France that the English crown holds.
He goes on to establish France as one of the premiere powers in the 13th century with the Battle of Bouvines in 1214.
To the east, in the lands of Flanders and Germany, Fredrick II (1212-1250) unifies the nobles of the Holy Roman Empire, the last vestige of Rome, but becomes embroiled in the conflicts and intrigues of the investiture controversy, pitting emperor against pope.
In the arid Iberian Peninsula, the kingdoms of Castile and Aragon are encroaching on the Moorish stronghold of al-Andalus.
Majorca falls in 1229,
Córdoba in 1236, and Seville in 1248, leaving Granada as the sole Moorish city until 1492.
Until then, it remains one of Europe's most learned cities, a center of knowledge and scholarship, surrounded and besieged by those who are jealous of its wealth and treasures.

The century is also one of heresy, and it witnesses the birth of the Papal Inquisition.
While the most gruesome excesses and witch-hunts are still centuries away, the Church prepares itself to deal with it enemies -- both mundane and supernatural.
The Albigensians of Southern France, who believe the world to be evil and preach a life of poverty, become the victims of the first crusade called on Christian lands.
Soon after, the Teutonic Knights march across Poland,
Hungary, Livonia and into Russia under the banner of their black cross, fighting pagans and other infidels in the name of God.
In the Holy Land, the century sees four different crusades head for various points along the Mediterranean.
For the most part, Christian forces suffer the familiar setbacks of the previous centuries, but these setbacks fail to deter one pope after another from calling for renewed efforts.

This is also a time when the east rides into the west.
The great Ghengis Khan dies in 1227, but his successors push into Europe.
They overrun Persia in 1231, southern Russia five years later, and they reach as far west as Poland and Hungary in 1241.
However, the khans and their horsemen are not the only travelers.
In 1271, the explorer Marco Polo leaves for the mysterious
East, traveling over lands that no westerner will see again for another five centuries.
When he returns, Europe is forever changed.

This, however, is the Europe of history books.

\begin{sideBox}

\subsection{Of History and Fantasy}

Set in AD 1230, the Dark Ages titles are historical games, deriving a good part of their dramatic energy and focus from real events -- the Albigensian Crusade, the conflicts of Emperor Fredrick II and the rekindling of the Guelph-Ghibelline conflict in Italy, to name a few.
The games' roots, however, stretch far back to the chaos that spread after the collapse of Rome, a time when a mighty empire fractured and splintered into the many kingdoms that rule Europe in the 13th century.
Historically, the ``Dark Ages'' refer to a short span of centuries, roughly from the fall of
Rome to the rise of Emperor Charlemagne in the ninth century.
However, the term captures the atmospheric merging of history and horror inherent to the Dark Medieval -- hence our anachronistic usage.

Although it is based on fact and history, the core of the Dark Ages setting rests in the uncharted spaces between the lines of the history books.
Although the lives of kings, popes and monarchs are well documented, we know very little beyond their castles and cities with any degree of certainty.
The Dark Medieval night is unknown and uncertain, and the fantastic elements of Dark
Ages emerge from this uncertainty.
This is not the fantasy of elves, dragons and wizards.
It is the wonder of a world uncharted, colored by the fear of the unknown.
This is a time of terror and daring, of unspeakable evils and unlimited opportunities.

\end{sideBox}

\subsection{By Night}

Superstition rules the Dark Medieval just as much as monarchs and popes do.
It is an age when religion and faith provide hope and salvation, but also preach that the Devil and his minions lurk in the night.
To the populace of Europe, this is not dogma but reality.
The people know that the dark forests hide more than just shadows, and they believe, just as they believe in God, that specters, demons and vampires exist.
In this age, farmers attend Mass, praying for forgiveness and salvation, then gather in fields to pay tribute and placate the Old Gods -- not because they have no faith, but because they absolutely and completely believe.
They believe that evil witches and warlocks can command the spirits of the dead, calling them from their resting-places to torment the living.
When returning from their pilgrimages, commoners and nobles alike have seen strange glyphs carved on stones and heard ethereal music when the moon was full.
They know that the Devil waits to tempt the faithful with sin, dooming them to an eternity of carnal torment and grief.

\section{Denizens of a Dark World}

The Dark Ages setting is home to a host of powerful beings, each driven by intrigues and struggles mostly hidden from mortal view.
These conflicts spill over into the human world as myths and folktales, explained away to disguise the often frightening truth: Humans are not alone in the world, and the shadowy creatures lurking in the dark are more than stories told to frighten children.

\ifvamp\else\index{Vampires}
  \subsection{Vampires}

  If the daylight world of the Dark Medieval is akin to our own history, its nighttime is a wholly different affair.
  Once the sun goes down and most God-fearing folk retire to the safety of homes or hovels, vampires come out.
  These blood-drinking creatures have existed from the dawn of time, and they rule the medieval night.
  They have courts and kings of their own, and they fight wars and negotiate treaties unknown to the living.

  These predators in dead flesh, these immortals, these lords of the night, take center stage in our theater of the macabre.

\fi

\index{Werewolves}
\subsection{Werewolves}

People have their legends.
They say that the wolves of the forest are the Devil's children, that they come in the night to steal human babies.
They say that spirits that can drive men mad haunt the woods.

The werewolves have their own legends, their own spirits and their own devils.
But unlike the humans, who believe in such things out of fear and tradition, the werewolves have seen the world of the spirits, and they know their place within it.
These creatures of Rage fight to protect the land and its spirits from corruption and taint, their tribes lurking just outside the boundaries of civilization.
It is their roles that you and your friends play in Dark Ages: Werewolf.

\index{Mages}
\subsection{Mages}

In medieval times, the prospect of a mage living down the road or chanting in midnight groves was still believed possible, even if few people ever actually could prove such an assertion.
In later years, the belief in wizards and warlocks takes on a fairy tale quality, a relic of a bygone age.

In the Dark Medieval, mages do stride the earth, calling down the will of Heaven or raising up the vengeance of the Earth for reasons all their own.
In a world ruled by hierarchy -- kings and
Popes -- mages break all the rules.
In Dark Ages: Mage, it's your turn to play one of those to whom the rules do not apply.

\index{Fae}\index{Changelings}
\subsection{Fae}

 Every human culture has legends and folktales of monsters, goblins, spirits and beasts.
 Some of the creatures are considered helpful, while others prey on humanity itself.
 Many such creatures are kind and cruel by turns, or are generally vicious but can be tricked or bested by the person who knows just how.

A long truce between the faerie Courts draws near an end, and an ages-old war threatens to begin anew.
In Dark Ages: Fae, you have the chance to play one of the Fair Folk at a crucial moment in faerie history -- will you help prevent the coming conflict, or help your Court crush its enemies?

\index{Inquisitors}
\subsection{Inquisitors}

The Papal Inquisition, as the history books reckon it, is still several years off.
However, in the Dark Medieval, the shadow Inquisition or Holy Inquisition \textit{contra Diabolum enim et alii daemons}
(against the Devil and other demons) rose in the first half of the 13th century to combat servants of Satan.

\section{The Storytelling Game}

Each of the titles in the \longSetting\ setting is a game -- more precisely a storytelling game.
With it, you and your friends cooperate to tell a tale (the storytelling part) and overcome challenges
(the game part).
It's essentially a game of make-believe, in which almost everyone portrays a specific character who grows and evolves thanks to his experiences.
Taken together, the experiences of all the characters form a story.

The players of a storytelling game are both the actors in the ``play'' and its audience.
Unlike most other games, the players are not playing against each other (or against anyone, in fact).
The objective is not to score a certain number of points or be the first to achieve a particular goal.
The players' objective is to have fun telling a good story and to have their characters get what they want.
There certainly will be opposition along the way -- from rivals and other threats -- but the players often cooperate to overcome those challenges.

One player takes on a special role and is called the storyteller.
She acts as the narrator of the story and plays the roles of all the other folks whom the players' characters might encounter.
So if the players decide that their characters enter a monastery, the Storyteller describes the monastery (including deciding just what it looks like, who's in it and whether it's busy or quiet.) and portrays the monks.
From the stoic prior who gives them a quick grunt to the gabby initiate who wants to talk about theology, it all comes out of the Storyteller's mouth.
In effect, the
Storyteller serves as the players' characters' senses.

\index{Storyteller}
The Storyteller then uses all this information to present challenges and opportunities to the players (through their characters).
The players then react, moving forward and creating a whole story.
Usually the Storyteller has a rough idea of the plot (or at least its set-up) in her mind ahead of time -- ``The monastery is supposedly haunted, but it is actually the home of a feral vampire who'll need to be reckoned with.'' -- but the details emerge from the players' actions.
Some might want to destroy the rival vampire and claim the monastery as their own domain.
Others might seek to befriend him or trade information for safe passage.
Thus, acting together, Storyteller and players create a unique tale.

\longSetting\ is a game because it uses rules to define how and what characters can do.
Unlike a board game, these rules are very loose, and every character can do a wide variety of things.
But when a chance for failure arises, the rules call for the use of dice to determine what happens.
Various traits (such as Strength, Etiquette and Courage) measure every character's capabilities, and players roll dice based on those traits.
Of course, much of the game happens without dice.
The best play often occurs with the Storyteller portraying an informant or ``bit part'' and engaging the players in lively conversation.
There's rarely a need to roll dice then.

\label{worldOfDarkness}
The world of \longSetting\ is not our own, though it is close enough for fearsome discomfort.
Rather, the world inhabited by vampires is like ours, but through a looking glass darkly.
Evil is palpable and ubiquitous in this world; the final nights are upon us, and the whole planet teeters on a razor's edge of tension.
It is a world of darkness.

Superficially, the World of Darkness is like the ``real'' world we all inhabit.
The same bands are popular, violence still plagues the inner city, graft and corruption infest the same governments, and society still looks to the same cities for its culture.
The World of Darkness has a Statue of Liberty, an Eiffel Tower and a CBGB's.
More present than in our world, though, is the undercurrent of horror -- our world's ills are all the more pronounced in the World of Darkness.
Our fears are more real.
Our governments are more degenerate.
Our ecosystem dies a bit more each night.
\ifvamp And vampires exist.

  Many of the differences between our world and the World of Darkness stem from these vampires.
  Ancient and inscrutable, the Kindred toy with humanity as a cat does with a trapped mouse.
  The immortal Kindred manipulate society to stave off the ennui and malaise that threaten them nightly, or to guard against the machinations of centuries-old rivals.
  Immortality is a curse to vampires, for they are locked in stagnant existences and dead bodies.

  This chapter examines the vampires' world.
  The World of Darkness reflects the passion and horror of its secret masters, and the hope of redemption is the only thing that lets most denizens of this cursed place go on living -- or unliving.
\else
  And monsters exist.
  
  These monsters do not live in a distant, haunted, forest, but among us, speaking to regular people, and directing humanity here and there as suits their needs.
  They control the newspapers and the churches, the history books and the wars.
  Humanity sits in a pen, ready to be used as tools or food.
\fi

\ifvamp
  \subsection{The World of Darkness}

  The greatest difference between our world and that of \longSetting\ is the presence of immortal monsters pulling the strings of humanity.
  Violence and despair are more common here, because they need to be in order for the
  Kindred to continue their existences.
  The world is bleak, but escape is an ever-present commodity -- perhaps too present.
  The setting of Vampire is a composite of its populace and their despair.
\fi

\subsubsection{Gothic-Punk, and Portents of the Future}

``Gothic-Punk'' is perhaps the best way to describe the physical nature of the World of Darkness.
The environment is a clashing mixture of styles and influences, and the tension caused by the juxtaposition of ethnicities, social classes and subcultures makes the world a vibrant, albeit dangerous, place.

The Gothic aspect describes the ambience of the World of Darkness.
Buttressed buildings loom overhead, bedecked with classical columns and grimacing gargoyles.
Residents are dwarfed by the sheer scale of architecture, lost amid the spires that seem to grope toward Heaven in an effort to escape the physical world.
The ranks of the Church swell, as mortals flock to any banner that offers them a hope of something better in the hereafter.
Likewise, cults flourish in the underground, promising power and redemption.
The institutions that control society are even more staid and conservative than they are in our world, for many in power prefer the evil of the world they know to the chaos engendered by change.
It is a divisive world of have and have-not, rich and poor, excess and squalor.

The Punk aspect is the lifestyle that many denizens of the World of Darkness have adopted.
In order to give their lives meaning, they rebel, crashing themselves against the crags of power.
Gangs prowl the streets and organized crime breeds in the underworld, reactions to the pointlessness of living ``by the book.'' Music is louder, faster, more violent or hypnotically monotonous, and supported by masses who find salvation in its escape.
Speech is coarser, fashion is bolder, art is more shocking, and technology brings it all to everyone at the click of a button.
The world is more corrupt, the people are spiritually bankrupt, and escapism often replaces hope.

\ifvamp
  As if this weren't fearful enough, the last few years have seen a quiet but pervasive dread grip the Kindred community.
  Many Kindred whisper of the Jyhad, the eternal war or game said to consume the most ancient vampires.
  This struggle has been waged since the dawn of time, but many vampires fear that, as one millennium passes to the next and the curse of undeath grows weaker, an apocalyptic endgame is at hand.
  Signs and portents, many recorded in the prophetic Book of Nod, trouble vampires of all clans and lineages, even those who profess not to believe.
  Whispers in Sabbat covens and Camarilla salons alike speak of turmoil in the East, of armies of Clanless rabble, of vampires whose blood is so thin that they cannot
  Embrace, of meetings with mysterious elders whose vast power betrays no discernible lineage, of black crescent moons and full moons red as blood.
  All, say the believers, are omens that the Final Nights are approaching, and that the end of all things is nigh.

  Some Kindred believe that a Reckoning is at hand, that the powers of Heaven are preparing at last to judge the vampires and what they have made of the world.
  Others speak of the Winnowing, or Gehenna, the night when the most ancient vampires will rise to consume their progeny, taking their lessers' cursed blood to sate their own hunger.
  Few admit to such superstitions, but most feel a palpable tension in these nights.
  Elder vampires play their hands in one fell swoop, negating centuries-long schemes in a single mad flurry of action.
  The warpacks of the dread Sabbat hurl themselves at the fortresses of their enemies, for they fear they might not get another opportunity.
  Cells of Assamite cannibals, formerly held in check by a great curse, hunt other vampires and ravenously drink their blood.
  Vampires of uncertain lineage are hunted down and destroyed by paranoid elders, who fear them as harbingers of Gehenna.
  Though patience is a special virtue among the immortals, it is practiced less and less, and the whole Kindred world teeters on the verge of a great collective frenzy.
\fi

Gothic-Punk is a mood and setting conveyed during the course of the game.
The greatest share of creating this ambience falls upon the Storyteller, but players should consider their characters' stake in it as well.
The ambience is also a matter of taste.
Some troupes may prefer more Gothic than Punk, while others may want equal amounts of both elements, or little of either.
In the end, it's your game, and you are free to make of it what you will.
Simply bear in mind that experiencing the world is a shared endeavour, and everything the players and Storyteller do helps make that world more believable.
Actions, settings, characters and descriptions all convey the Gothic-Punk aesthetic.

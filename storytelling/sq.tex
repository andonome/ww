% This files has all the examples we'll use for storyweaving.
\input{storytelling/examples.tex}

\section{Story Weaving}
\label{weavingStories}
\index{Stories}
\index{Story Weaving}

Writing stories is hard.
We come up with a scene here and there, ideas for what players might do, but leading them into a larger whole either seems too easy (and the players divert all those `simple' plans), or too hard (and the chronicle begins without clear planning).
This system will help anyone write and tell immersive stories which can adjust fluidly to players' reactions.

It starts with a single scene.

\begin{enumerate}
  \propScene
\end{enumerate}

It could be a nice conversation piece, but it doesn't present enough to discuss yet, because it has no context.
And it certainly does not form a story.

\subsection{The Strands}
\label{strands}
\index{Strands of Stories}

We build even the grandest stories from a series of tiny strands.
By linking these pieces together, scene after scene, a story will emerge.
Each part must relate to the whole, but does not necessarily have to relate to scene immediately following.
Having a `B-plot', (or even `C' and `D') is fine, and characters don't need to know which scene is part of the main plot and what is an aside until after (in fact even the Storyteller might not know until sometime later).

\subsubsection{Plans and Shadows}

Before adding another part, the scene needs background.
We can establish that \ifvamp the local Prince is a despot\else a trickster is in town \fi sometime earlier.

\begin{enumerate}
  \setupScene
  \propScene
\end{enumerate}

The scene can be fleshed out later -- \ifvamp the Ravnos might use Chimestry to attempt escape, and the players might aid him, or not\else\ifDA more people might have bought gifts which aren't there, or the woman might accuse someone nearby of theft\else the police might arrive, prompting the street-magician to flee\fi\fi.
But during the early writing, we can just focus on the overarching plot elements.
\ifvamp
  Currently it establishes why the coterie might fear or hate (or love) the prince, and gives them an opportunity to move against him (or aid him).
\else
  Currently, it establishes that a trickster is in town, and is wanted by authorities.
\fi

These compact lists of a few scenes make it easy to set up foreshadowing and set up background information.

\subsubsection{Conclusions, Not Promises}

Players should have agency within a scene, but that does not mean they can determine the future.
Individual scenes should not depend upon each other if it can be helped.
Player decisions can alter how a scene plays out, and will certainly change the end of a story -- or even create it entirely -- but most scenes should plod along no matter what happens, and no matter how the players react.

Always take care when writing a scene that it does not depend on specific results from previous scenes.

\begin{enumerate}
  \setupScene
  \propScene
  \spyScene
\end{enumerate}

\ifvamp
  The harpy might tell the coterie that the prince relies on a steady stream of blood from a particular blood-bank to feed his various ghouls, and pay an Assamite assassin.
  They can relay this back to the distant prince from the second city, or may spot the lie.

  If they spot the lie, they might try to kill the harpy, or they could simply deny all knowledge.
\else
  If the crew approach their contact, they find the \ifDA trickster \else street magician \fi asleep in a bed, but if they try to wake him, they find he was nothing but an illusion.
  The real trickster went to sleep under the bed, leaving his fake there (he wants to know if he would have remained undisturbed while sleeping).
\fi

Any scene which ends with an obvious and definitive reaction does not need to be spelled out -- if we know how the player characters, or \ifvamp harpy \else poet \fi will react, there is no need to specify this in the story-thread.
\ifvamp
  If the prince discovers the coterie are working against him, he may call a blood hunt, or may think of some further use for them.
  Whether or not any of that happens, we know a battle is brewing, and the distant prince will be making  other moves.
\else
  If the group manage to find and imprison the mysterious guest who summons illusions from nothing, you can assess his best moves at the time.
\fi

Once a story ends, the troupe gains Experience Points (see page \pageref{experience}), even if only one of many stories have resolved.

\subsection{The Tapestry}

We can weave multiple stories together in order to create a larger tale.
Each individual story must weave in an out of different areas, but first each needs to go into an area.

The exact areas depend upon your chronicle, but a nice default set might include `City Centre', \ifDA `Docks', `Forest', \else `Slums', `Suburbs',\fi and \ifvamp `Elysium'\else `Home'\fi.


\begin{enumerate}
  \setupScene
  \propScene
  \spyScene
  \sheriffScene
  \ghoulScene
  \newcommerScene
  \lastScene
\end{enumerate}

\ifvamp
  The local sheriff will, of course, be a powerful kindred -- perhaps an ancilla Brujah.
  However, right now he is running low on blood, and in dire need.
  This presents an opportunity for the coterie.
  They are within their rights  to request a favour, or might decide to coax him back to their haven and kill him.
\else
  The `magician' is, of course, some supernatural creature, so the group may feel reticent to chase him down, or struggle understanding what the limits of his abilities are.
\fi

Note that each scene has been assigned an area.
Once the characters enter that area, the scene activates, and not before.
If the players do not enter that area, the story does not continue without them under normal circumstances (although some scenes may be time-sensitive).
If the \ifvamp coterie \else group \fi leave an area, they can continue with a different story, and a different scene, or start a new one.
The original will be waiting for them when they return.

The areas you decide to use will form the focus of the entire chronicle.
If something happens every time the coterie enter the \ifDA guild district\else sewers\fi, it will become a place of interest.
If the local forest contains nothing, then the coterie could journey there, but they will find little to nothing to do.
Conversely, if you create the Tremere chantry as an official location, the group will come to think of  it as a special location after a scene or two has played out there.
Players will still need a reason to go somewhere -- it could be that one scene calls the next, or that the area simply lets them hunt easily -- but the very fact that the action always happens somewhere is enough to engage the players with the area.

Locations also show the scope of a chronicle.
A chronicle focussed on the wide-scale war between the \ifDA Franks and the Germans\else Sabbat and the Camarilla \fi might use three cities as a location, and use the \ifDA borer lands\else barren wastelands \fi between them as a fourth.
In this case, each scene will have to work anywhere in that city, or be amenable to enough locations within the city, such as a scene which takes place in the coterie's haven (wherever that happens to be).

Other chronicles might focus entirely on Gangrel vs the \ifDA lupines\else local Giovanni\fi, with locations including \ifDA `river', `mountain', `villages', and `town'\else `58th St.', `38th St', `the Club', and `the Giovanni Mansion' (where the most dangerous scenes take place)\fi.
Having so much activity within this narrow area demands that the players think small-scale.

\subsection{Crossing Strands}

Every chronicle should contain multiple stories -- at least five, but easily many more.
Have each story populate the various areas laid out for your chronicle.
An individual story may only populate one area.
Other could shift continuously from one area to the next.

\ifvamp
  \vampStoryTwo
\else
  \ifDA
    \DAStoryTwo
  \else
    \WoDStoryTwo
  \fi
\fi

Scenes marked with an `\textit{X}' are \emph{crossover scenes}.
They cross over into the next story, altering whatever occurs there.

\ifvamp
  The scene with the sheriff asking for the characters' aid may occur just as the \ifDA monsters invade the village\else lights go out all over the slums\fi.
  The scene with the strange noise in the sewers may occur at the same time as the prince's ghoul flees out, covered in scars.
  The players will not immediately understand what relates to what, and will have to engage with the chaotic scene before them to understand everything that has happened (or even some of what has happened).
\else
  The initial scene with the \ifDA gossip about the cat \else newspaper \fi isn't a full scene -- if players ever hear nothing but `you \ifDA hear about a cat \else read a newspaper and it says this\fi', then they will take the hint, but they will not like it.
  As a stand-alone fact, it simply bashes players over the head with with `PLOT PLOT PLOT'.
  However, once blended into another scene, it can fade into the background, to be later recalled when needed.
  \ifDA
    The last scene, where someone finally finds the cat adds an element of chaos, because we don't know which other scene it might merge with.
    Perhaps they find the cat when trying to stalk someone, and have to quietly woo it with food before a random beggar scares it away, or perhaps a fight breaks out and someone grabs it -- they players then have to engage in the fight without harming the cat, and ensure it doesn't run.
  \else
    For example, the scene might run with one above in the Town Centre, where the gang-leader approaches them to ask about the street magician.
    Perhaps it's on the TV in a bar, or maybe one is reading a newspaper with the story, when he approaches them.
  \fi
\fi

Crossing over scenes allows different plot-arcs to interact seamlessly.
\ifvamp
  Perhaps the \ifDA Baali\else Sabbat\fi will try to kill the characters while they investigate \ifDA lupines in the area\else a local gang\fi, or perhaps a \ifDA Tzimisce\else Sabbat \fi spy has already befriended whoever else is in the scene, using an alias.
\else
  \ifDA
    Perhaps a war is brewing while the troupe are hunting for a witch, and they find her in a village at exactly the moment an army enters, or perhaps an eclipse occurs (heralding bad things to come), while the group have to flee the baron's soldiers.
  \else
    Perhaps the mob boss approaches the crew to make a proposition, but the police from another story-strand start banging on the door, or perhaps a football riot breaks out in the city enter, exactly when the crew run into the lost child they had been searching for.
  \fi
\fi

Crossover scenes serve a variety of functions.
Some scenes don't really form a scene -- if someone \ifDA hears news of \else finds a poster for \fi a missing child, that doesn't give much to interact with, so it may as well be relegated to a crossover scene, so the players will have something substantial to do, and the news-item can appear as part of the general description, or as a throwaway line which the group may (or may not) remember later.
These little details can serve as excellent foreshadowing, without spelling the plot out in neon lights.

At other times, a grand scene might become even grander as a crossover scene -- whatever else happens in this area happens at the same time, so a \emph{lot} will be happening.
A normal brawl becomes an insane mess, as the \ifvamp coterie \else group \fi suddenly has to deal with that brawl plus whatever else some random plot demands.

Crossover scenes can easily create the tempo of an environment.
If all the scenes in the \ifDA forest \else sewers \fi occur alone, while the scenes in the center of town always  have some counterpart, the center will feel like a demanding, chaotic, mess, while the \ifDA forest \else sewers \fi will feel more tranquil and empty, as one thing happens at a time.

\subsubsection{Mister `X'}

Some scenes may use stand-in characters, such as `one of the \ifvamp coterie\else group\fi's allies or contacts', or \ifvamp `one of the prince's ghouls'\else `a recent enemy\fi.
Adding in the group's individual friends and enemies will make the scenes more relevant and  fitting for the troupe.
You may also have to adjust scenes to fit this pattern if the \ifvamp coterie\else troupe \fi kill someone, or destroy their reputation, before a scene can play out.
No plan can withstand collision with the players for long, so even seemingly independent scenes will require some adjustment from time to time.

\subsection{Final Stitches}

Some stories paint their final scene in black and white.
\ifvamp
  Either the Malkavian finds her long-lost doll, or she doesn't, and any fight between two princes should end with one dead.
\else
  Either the gambler wins his bet or he doesn't, and \ifDA peace treaties are either signed or not\else bombs either explode or don't\fi.
\fi
However, the final scene does not always need to be written.
Once enough pieces are in motion, the plot often takes on a life of its own; players will want to gain certain information, local \ifvamp Cainites \else antagonists \fi will start various Contests, which will (hopefully) bring the plot to a reasonable conclusion.

However the story ends, just be sure to end it in such a way that the players understand that it has truly concluded.%
\footnote{Unless, of course, you want to go for a surprise last scene, sometime later, with an even bigger reveal.}

\subsubsection{Cutting Loose Ends}

Sometimes a story dies prematurely, and that's okay.
If your chronicle has 10 stories, and one loses a few scenes because the \ifvamp coterie found a clever way to make peace with the sheriff\else group somehow managed to kill the local ruffian\fi, just leave it, award Experience Points early, and move on.
Not every story has to be an epic saga.

\subsubsection{Primary Plots}

In order to emphasize something as a `main plot', you can either give it more scenes, or keep the plot within the same area.
A plot which dominates the local \ifvamp Elysium\else \ifDA harbour \else clubs \fi\fi will return night after night, while a story about a wandering \ifvamp Ravnos\else gypsy \fi, who changes to a new area every single scene, will automatically feel like a drawn-out journey, as the group encounter it only when they go to the right place.

You can force a plot to be `the primary plot', by having multiple threads point in the same direction.
You might \ifDA stage multiple threads, each of which result in retrieving a single fragment of the \ifvamp Book of Nod\else Bible's apocrypha\fi \else write five stories, each concerned with a different \ifvamp Sabbat character \else gang \fi infiltrating the city\fi.

You have many ways to push, but no guarantees -- once you start telling the story, the players will determine which plot is primary, because the real \emph{primary} plot, is always the plot the players decide is primary.
Even if you have written three stories about \ifvamp losing one's humanity \else corruption in the church \fi, the troupe may end up focussed exclusively on your side-plot about \ifvamp the Followers of Set\else strange weather conditions\fi.
If this happens, that's fine -- let them focus on it, and leave scenes to wander out where they may.

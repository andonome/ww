
\subsubsection{Torpor}
\label{torpor}

Character enters a deathlike trance.
He may do nothing, not even spend blood, until a certain period of time has passed.
Final Death Character dies again, this time forever.

Incapacitated: The stage immediately before torpor, incapacitation differs from unconsciousness in that your character collapses from the combined effects of physical trauma and pain.
She falls to the ground and may do nothing except spend blood points to heal damage.
Further damage suffered by an incapacitated vampire sends her into torpor or, if the damage is aggravated, inflicts Final Death on her.

Torpor: Torpor is the deathlike sleep common to the undead, particularly among ancient vampires.
Torpor may be entered voluntarily (certain undead, weary of the current age, enter torpor in hopes if reawakening in a more hospitable time) or involuntarily (through wounds or loss of blood).
Once in torpor, a character remains dormant for a period of time depending on her Humanity rating.

As mentioned, characters with zero blood points in their blood pools begin to lose health levels each time the rules call for them to spend blood.
When a vampire falls below Incapacitated in this fashion, she enters torpor.
There she will remain until someone feeds her at least a blood point.
If this happens, she may rise, regardless of Humanity rating.
This sort of revivification works only for vampires who enter torpor from blood loss.

Vampires who enter torpor due to wounds must rest for a period depending on their Humanity rating:

\begin{boxTable}[cX]{}

\textbf{Humanity} & \textbf{Length of Torpor} \\
10 & One day \\
9 & Three days \\
8 & One week \\
7 & Two weeks \\
6 & One month \\
5 & One year \\
4 & One decade \\
3 & Five decades \\
2 & One century \\
1 & Five centuries \\
0 & Millennium+ \\
\end{boxTable}

Following this period of rest, the player may spend a blood point and make an Awakening roll (p. \pageref{awakening}) for her character to rise.  If the vampire has no blood in her body, she may not rise until she is fed; if the player fails the Awakening roll, she may spend another blood point and make an Awakening roll the following night.
If the vampire rises successfully, she is considered Crippled and should either spend blood or hunt immediately.

A character may enter torpor voluntarily.
This state resembles the character's normal daily rest, but is a deeper form of slumber and should not be entered into lightly.
A vampire in voluntary torpor may rise after half the mandatory time period for involuntary torpor, but must make an Awakening roll to do so.
A torpid vampire may ignore the nightly need for blood; she is effectively in hibernation.

Mortals have no torpor rating; if reduced below Incapacitated, they simply die.

Final Death: If a vampire is at the Incapacitated health level or in torpor and takes one more level of aggravated damage, he dies permanently and finally.
A player's character who meets Final Death is out of the game; the player must create a new character if she wishes to continue play.

An incapacitated or torpid vampire may also be sent to Final Death through massive amounts of bashing or lethal trauma
(decapitated, trapped under a 10-ton rock, fed into a wood chipper, caught at ground zero of an explosion, crushed by deep- sea pressure, etc.).
Typically, this damage must be enough to destroy or dismember the corpse beyond repair.

\subsubsection{Blood \ifDA Oath\else Bond\fi}
\label{bloodBond}
\ifDA\index{Blood!Oath}\index{Oath}\else\index{Blood!Bond}\fi

One of the most wondrous and terrible properties of Kindred vitae is its ability to enslave nearly any being who drinks of it three times.
Each sip of a particular Kindred's blood gives the Kindred in question a greater emotional hold over the drinker.
If a being drinks three times, on three separate nights, from the same Kindred, she falls victim to a state known as the blood \bond.
A vampire who holds a blood \bond over another being is said to be that victim's regnant, while the being subordinate to the \bond is called the thrall.

Put simply, blood \bond is one of the most potent emotional sensations known.
A blood bound victim is absolutely devoted to her regnant and will do nearly anything for him.
Even the most potent uses of Dominate cannot overcome the thrall's feelings for her regnant; only true love stands a chance against the \bond, and even that is not a sure thing.

The blood \bond is most commonly used to ensnare mortals and ghouls, but Kindred can bind each other as well.
Such is the blood \bond's power that a mighty elder can be bound to a lowly neonate; in this respect, the blood of a 13th-generation fledgling is (presumably) as strong as that of Caine himself.
As such, the blood \bond forms an essential strategy in the
Jyhad; some Ancients are said to hold dozens of influential Kindred in secret thralldom.

\paragraph{First drink:}
The drinker begins to experience intermittent but strong feelings about the vampire.
She may dream of him, or find herself ``coincidentally'' frequenting places where he might show up.
There is no mechanical effect at this stage, but it should be roleplayed.
All childer have this level of \bond toward their sires, for the Embrace itself forces one drink upon the childer; they may love their ``parents,'' hate them, or both, but are rarely indifferent toward them.

\paragraph{Second drink:}
The drinker's feelings grow strong enough to influence her behavior.
Though she is by no means enslaved to the vampire, he is definitely an important figure in her life.
She may act as she pleases, but might have to make a Willpower roll to take actions directly harmful to the vampire.
The vampire's influence is such that he can persuade or command her with little effort (Social rolls against the thrall are at -1 difficulty).

\paragraph{Third drink:}
Full-scale blood \bond.
At this level, the drinker is more or less completely bound to the vampire.
He is the most important person in her life; lovers, relatives and even children become tertiary to her all-consuming passion.

At this level, a regnant may use the Dominate Discipline on a thrall, even without the benefit of eye contact.
Merely hearing the regnant's voice is enough.
Additionally, should the thrall try to resist the Dominate for some reason, the difficulty of such resistance is increased by two.
Naturally, a higher-generation vampire still cannot use Dominate on a lower-generation thrall.

The blood \bond\ is true love, albeit a twisted and perverse version of it.
Ultimately, we can't reduce the vagaries of love down to a simple ``yes/no'' system.
Some thralls (particularly people with Conformist or other dependent Natures or with
Willpower 5 or less) will commit any act, including suicide or murder, for their beloved; other characters have certain core principles that they will not violate.

A full blood \bond, once formed, is nearly inviolate.
Once bound, a thrall is under the sway of her regnant and her regnant only.
She cannot be bound again by another vampire unless the first blood \bond wears away ``naturally''.
A vampire can experience lesser (one- and two-drink) \bond s toward several individuals; indeed, many Kindred enjoy such \bond s, as they create artificial passion in their dead hearts.
Upon the formation of a full blood \bond, though, all lesser sensations are wiped away.
Vampire lovers occasionally enter into mutual blood \bond s with each other; this is the closest thing the undead can feel to true love.
Even this sensation can turn to disgust or hate over the centuries, though, and in any event few Kindred are trusting enough to initiate it.
A blood \bond\ is a mighty force, but it is at its most potent when perpetually reinforced with further drinks.
Feeding a thrall often reinforces the \bond\, while depriving a thrall of vitae may cause the \bond\ to grow tepid over time.
As well, like any other relationship, treatment and courtesy play a part in the dynamics of the \bond.
A thrall who is treated well and fed often will likely fall even more deeply in love, while a thrall who is degraded and humiliated may find resentment and anger eating away at the \bond.

It is possible, though difficult, for a vampire to temporarily resist a blood \bond.
Doing so requires the player to make a
Willpower roll (difficulty is typically 8, though this can be modified depending on the regnant's treatment and the thrall's
Nature) and accumulate a number of successes equal to the number of times the thrall has partaken of the regnant's blood.
The thrall must then spend a Willpower point.
Upon doing so, the \bond\ is negated for a variable amount of time: from one scene (if the thrall merely wishes to plot against the regnant, deliver confidential information to an enemy, etc.) to one turn
(if the thrall wishes to attack the regnant physically).
The thrall can continue to expend Willpower to extend the duration of
``freedom,'' but once she ceases doing so, the blood \bond\ resumes at full force.

A blood \bond\ can be broken, though this requires the thrall to not only avoid the regnant entirely for an extended period of time, but also spend great amounts of Willpower to overcome the ``addiction''.
As a general rule, a thrall who neither sees nor feeds from her regnant for a period of (12 -- Willpower) months finds her \bond\ reduced by one level (so, a fully bound thrall with a Willpower of 5 has her blood \bond\ reduced to the equivalent of two drinks if she goes seven straight months without any contact with the regnant).
If the \bond\ is reduced to zero in this fashion (a feat typically accompanied by the expenditure of a great deal of Willpower on the thrall's part, as she resists the gnawing urge to seek out her sire), it is nullified entirely.

Another, though somewhat less certain, way to be rid of the \bond\ is to kill the regnant.
Such a choice is extremely perilous on many levels, and makes no guarantees that everything will go smoothly.
Those who have been released by such means claim the \bond\ shatters like spun glass upon the moment of the regnant's Final Death.
The thrall's Nature may play a large part in whether the control is completely ended, though, and such aftermath is best left in the hands of the Storyteller.

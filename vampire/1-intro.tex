\chapter{Introduction}
\begin{multicols}{2}

Vampires.

Bloodsucking corpses returned from the grave to feast on the blood of the living.
Monsters damned to Hell who avoid their punishment through life unlawfully stolen.
Erotic predators who take their sustenance from innocent, struggling -- or, perhaps, willing? -- men and women.

Since time's beginning, humanity has spoken of the vampire -- the undead, the demonic spirit embodied in human flesh, the corpse risen from its grave possessed of a burning hunger for warm blood.
From Hungary to Hong Kong, from New Delhi to
New York, people throughout the world have experienced chills of delicious terror contemplating the deeds of the night-
stalking vampire.
The vampire has haunted novels, movies, TV series, video games, clothing, even breakfast cereal.

But these stories are mere myths, right?

Wrong.

Vampires have walked among us from prehistoric times.
They walk among us still.
They have fought a great and secret war since the earliest nights of human history.
And this eternal struggle's final outcome may determine humanity's future -- or its ultimate damnation.

\subsubsection{Storytelling}

The book you hold is the core rulebook of \setting, a storytelling game from White Wolf Publishing.
With the rules in this book, you and your friends can take the roles of vampires and tell stories about the characters' triumphs, failures, dark deeds and glimmerings of goodness.

In a storytelling game, players create characters using the rules in this book, then take those characters through dramas and adventures, called (appropriately enough) stories.
Stories are told through a combination of the wishes of the players and the directives of the Storyteller (see below).

In a lot of ways, storytelling resembles games such as How to Host a Murder.
Each player takes the role of a character -- in this case, a vampire -- and engages in a form of improvisational theatre, saying what the vampire would say and describing what the vampire would do.
Most of this process is freeform -- players can have their characters say or do whatever they like, so long as the dialogue or actions are consistent with the character's personality and abilities.
However, certain actions are best adjudicated through the use of dice and the rules presented in this book.

Whenever rules and story conflict, the story wins.
Use the rules only as much -- or preferably as little -- as you need to tell thrilling stories of terror, action and romance.

\subsubsection{Players and Storytellers}

Vampire is best played with a group, or troupe, of two to six participants.
Most of these people are to be players.
They create vampire characters -- imaginary protagonists similar to those found in novels, cinema and comics.
In each troupe, however, one person must take the role of the Storyteller.
The Storyteller does not create one primary character for herself.
Rather, she acts as a combination of director, moderator, narrator and referee.
The Storyteller invents the drama through which the players direct their characters, creating plots and conflicts from her imagination.
The Storyteller also takes the roles of supporting cast -- allies with whom the characters interact and antagonists against whom the characters fight.
The
Storyteller invents the salient details of the story setting -- the bars, nightclubs, businesses and other institutions the characters frequent.
The players decide how their characters react to the situations in the game, but it is the Storyteller (with the help of the rules) who decides if the characters actually succeed in their endeavors and, if so, how well.
Ultimately, the
Storyteller is the final authority on the events that take place in the game.


\begin{example}[Example:]

  Rob, Brian, Cynthia and Alison have gathered to play Vampire.
  Rob, Brian and Cynthia are players: Rob is playing Baron d'Havilland, a Ventrue aristocrat; Brian is playing Palpa, a Nosferatu sewer-dweller; and Cynthia is playing Maxine, a Brujah street punk.
  Alison is the Storyteller, and she has decreed that the characters have been brought before the vampire prince of the city to face judgement for feeding in a forbidden area of the city.

  -- Alison (describing the scene):
  \begin{quotation}
    The room into which you are dragged is large, opulent, and filled with mementos from eras ranging from the Italian Renaissance to the Harlem Renaissance.
    A great iron chandelier, filled with candles, throws dim light on alcoves filled with paintings and statuary.
    Still, you don't have too much time to look, for at the prodding of the prince's enforcer, Lord Maxwell, you are rudely shoved before a great oaken chair -- almost a throne.
    The shadows seem to cluster more deeply about the imposing figure that sits in state, commanding, unmoving, and gazing at you with burning eyes.
  \end{quotation}

  -- Alison (again, now speaking as the prince):
  \begin{quotation}
    ``For over a century have I kept order in my domain.
    Like a careful gardener, I have watched this city grow from a rural town to a thriving metropolis.
    I have bested anarchs and squelched the plots of the Black Hand.
    Through what temerity do you newborn whelps now choose to flout my rule?
    Speak quickly and well, lest I personally stake you and leave you for the sun's kiss!''
  \end{quotation}

  The players may now decide what to do:

  -- Rob (speaking as Baron d'Havilland):
  \begin{quotation}
    ``Milord, clearly there has been a misunderstanding, as my colleagues and I\ldots 
  \end{quotation}

  -- Cynthia (speaking as Maxine):
  \begin{quotation}
    ``Don't presume to speak for me, you weak-blooded toady! To hell with your rules, you fascist prick! This is the 20th century, and I'll go anywhere I damn well choose!''
  \end{quotation}

  -- Alison (playing the prince's none-too-amused reaction):
  \begin{quotation}
    The prince's fingers noticeably tighten on the arms of the throne, and a low hiss rises in his throat.
  \end{quotation}

  -- Brian (describing his character's action):
  \begin{quotation}
    Brilliant, Maxine! I avoid the prince's gaze and look around.
    Is there any sort of pillar, shadow or other place that I can unobtrusively slip behind? If so, I'm going to use my Obfuscate (a magical invisibility power) to get the hell out of here.
  \end{quotation}

\end{example}

What happens next is decided by the actions of the players and the decisions of the Storyteller.
As you can see, each player is the arbiter of his or her own character's actions and words.
Ultimately, though, it is Alison, the Storyteller, who determines the prince's reaction to the characters' words or actions; it is Alison, speaking as the prince, who roleplays the prince's reaction; and it is Alison who determines whether the characters' actions, if any, succeed or fail.

\subsection{What is a Vampire?}

Storytelling and roleplaying games may feature many kinds of protagonists.
In some games, players assume the roles of heroes in a fantasy world, or superheroes saving the world from villains' depredations.
In Vampire, appropriately enough, players assume the personas of vampires -- the immortal bloodsuckers of the horror genre -- and guide these characters through a world virtually identical to our own.

The vampires who walk the earth in modern nights -- or Kindred, as they commonly call themselves -- are both similar to and different from what we might expect.
It is perhaps best to begin our discussion of the undead as if they were a separate species of being -- sentient, with superficial similarities to the humans they once were, but displaying a myriad physiological and psychological differences.

In many ways, vampires resemble the familiar monsters of myth and cinema. (There is enough truth in the old tales that perhaps they were created by deluded or confused mortals.) However -- as many an intrepid vampire-hunter has learned to his sorrow -- not all of the old wives' tales about vampires are true.


\subparagraph{Vampires are immortal.}
True.
While they can be killed (a very difficult process), they do not age or die from natural causes.
They need no food such as humans eat, and they do not need to breathe.

\subparagraph{Vampires are living dead and must sustain themselves with the blood of the living.}
True.
A vampire is clinically dead -- its heart does not beat, it does not breathe, its skin is cold, it does not age -- and yet it thinks, and walks, and plans, and speaks\ldots  hunts, and kills.
To sustain its artificial immortality, the vampire must periodically consume blood, preferably human blood.
Some penitent vampires eke out an existence from animal blood, and some ancient vampires must hunt and kill others of their kind to nourish themselves, but most vampires indeed consume the blood of their former species.

Vampires drain their prey of blood through the use of retractable fangs, which are magically gifted to vampires when they first become undead.
Each vampire can also magically lick closed the wounds made by their fangs, thus concealing the evidence of their feeding.

Blood is all-important to the Kindred, for it is both the crux of their existence and the seat of their power.
Mortal food, mortal air, mortal love -- all of these things are meaningless to a vampire.
Blood is the Kindred's only passion, and without it, they will quickly wither and fall dormant.
Moreover, each vampire can use its stolen blood to perform amazing feats of healing, strength and other magic.

\subparagraph{Anyone who dies from a vampire's bite rises to become a vampire.}
False.
If this were true, the world would be overrun with vampires.
Vampires feed on human blood, true, and sometimes kill their prey -- but most humans who die from a vampire's attack simply perish.
To return as undead, the victim must be drained of blood and subsequently be fed a bit of the attacking vampire's blood.
This process, called the Embrace, causes the mystical transformation from human to undead.

\subparagraph{Vampires are monsters -- demonic spirits embodied in corpses.}
False\ldots  and true.
Vampires are not demons per se, but a combination of tragic factors draws them inexorably toward wicked deeds.
In the beginning, the newly created vampire thinks and acts much as she did while living.
She does not immediately turn into an evil, sadistic monster.
However, the vampire soon discovers her overpowering hunger for blood, and realizes that her existence depends on feeding on her species.
In many ways, the vampire's mindset changes -- she adopts a set of attitudes less suited to a communal omnivore and more befitting a solitary predator.

At first reluctant to kill, the vampire is finally forced into murder by circumstance or need -- and killing becomes easier as the years pass.
Realizing that she herself is untrustworthy, she ceases to trust others.
Realizing that she is different, she walls herself away from the mortal world.
Realizing that her existence depends on secrecy and control, she becomes a manipulative user of the first order.
And things only degenerate as the years turn to decades and then centuries, and the vampire kills over and over, and sees the people she loved age and die.
Human life, so short and cheap in comparison to hers, becomes of less and less value, until the mortal ``herd'' around her means no more to her than a swarm of annoying insects.
Vampire elders are among the most jaded, unfeeling, paranoid -- in short, monstrous -- beings the world has ever known.
Maybe they are not demons exactly -- but at that point, who can tell the difference?

\subparagraph{Vampires are burned by sunlight.}
True.
Vampires must avoid the sun or die, though a few can bear sunlight's touch for a very short period of time.
Vampires are nocturnal creatures, and most find it extremely difficult to remain awake during the day, even within sheltered areas.

\subparagraph{Vampires are repulsed by garlic and running water.}
False.
These are myths and nothing more.

\subparagraph{Vampires are repulsed by crosses and other holy symbols.}
This is generally false.
However, if the wielder of the symbol has great faith in the power it represents, a vampire may suffer ill effects from the brandishing of the symbol.

\subparagraph{Vampires die from a stake through the heart.}
False.
However, a wooden stake -- or arrow, crossbow bolt, etc. -- through the heart will paralyze the monster until it is removed.

\subparagraph{Vampires have the strength of 10 men; they can command wolves and bats; they can hypnotize the living and heal even the most grievous wounds.}
True and false.
The power of a vampire increases with age.
Young, newly created vampires are often little more powerful than humans.
But as a vampire grows in age and understanding, she learns to use her blood to evoke secret magical powers, which vampires call Disciplines.
Powerful elders are often the rivals of a fictional
Lestat or Dracula, and the true ancients -- the Methuselahs and Antediluvians who have stalked the nights for thousands of years -- often possess literally godlike power.

\subsection{The Hunt}

When all is said and done, the most fundamental difference between humans and vampires lies in their methods of sustenance.
Vampires may not subsist on mortal food; instead, they must maintain their eternal lives through the consumption of blood -- fresh human blood.

Vampires acquire their sustenance in many fashions.
Some cultivate ``herds'' of willing mortals, who cherish the ecstasy of the vampire's Kiss.
Some creep into houses by night, feeding from sleeping humans.
Some stalk the mortals' playgrounds -- the nightclubs, bars and theaters -- enticing mortals into illicit liaisons and disguising their predation as acts of passion.
And yet others take their nourishment in the most ancient fashion -- stalking, attacking and incapacitating (or even killing) mortals who wander too far into lonely nocturnal alleys and empty lots.

\section{The Nocturnal World of the Vampires}

Vampires value power, for its own sake and for the security it brings -- and they find it ridiculously easy to acquire mundane goods, riches and influence.
A mesmerizing glance and a few words provide a cunning vampire with access to all the wealth, power and servants he could desire.
Some powerful vampires are capable of implanting posthypnotic suggestions or commands in mortals' minds, then causing the mortals to forget the vampire's presence.
In this way, vampires can easily acquire legions of unwitting slaves.
More than a few ``public servants'' and corporate barons secretly answer to vampire masters.

Though there are exceptions, vampires tend to remain close to the cities.
The city provides countless opportunities for predation, liaisons and politicking -- and the wilderness often proves dangerous.
The wilds are the home of the Lupines, the werewolves, who are vampires' ancestral enemies and desire nothing more than to destroy vampires outright.

\subsubsection{The Jyhad}

Many vampires seek to have nothing to do with their kind, choosing instead to exist and hunt in solitude.
However, the civilization of the undead is a manipulative and poisonous dance, and few vampires are left entirely untouched.
Since the nights of antiquity, the Kindred have stmggled for supremacy, in an ancient and many-layered struggle known as the Jyhad.
Leaders, cultures, nations and armies have all been pawns in the secret war, and vampiric conspiracies have influenced much (though by no means all) of human history.
Few things are as they seem in the vampires' nocturnal world; a political coup, economic crash or social trend may be merely the surface manifestation veiling a centuries-old struggle.
Vampire elders command from the shadows, manipulating mortals and other vampires alike -- and the elders are often manipulated in turn.
Indeed, most combatants may not even realize for whom they fight, or why.

Reputedly begun millennia ago, the Jyhad rages even today.
Though skyscrapers take the place of castles, machine-guns and missiles replace swords and torches, and stock portfolios substitute for vaults of gold, the game remains the same.
Kindred battles Kindred, clan battles clan, sect battles sect, as they have for eons.
Vampiric feuds begun during the nights of
Charlemagne play themselves out on the streets of New York City; an insult whispered in the court of the Sun King may find itself answered by a corporate takeover in Sao Paolo.
The ever-swelling cities provide countless opportunities for feeding, powermongering -- and war.

Increasingly, vampires speak of Gehenna -- the long-prophesied night of apocalypse when the most ancient vampires, the mythical Antediluvians, will rise from their hidden lairs to devour all the younger vampires.
This Gehenna, so the Kindred say, will presage the end of the world, as vampires and mortals alike are consumed in an inexorable tide of blood.
Some vampires strive to prevent Gehenna, some fatalistically await it, and still others consider it a myth.
Those who believe in Gehenna, however, say that the end time comes very soon -- perhaps in a matter of years.

\section{How to Use This Book}

This book is divided into several chapters, each of which is designed to explore and explain a specific area of the game.
Remember, though, that in a storytelling game, the most important ``chapter'' is your imagination.
Never let anything in this book be a substitute for your own creativity.

\autoref{worldOfDarkness}: A World of Darkness describes the Kindred and the world in which they hunt.

\autoref{clans}: Clans and Sects describes the 13 great ``clans'' of Kindred and the organizations to which they hold allegiance.

\autoref{characterCreation}: Character gives step-by-step instructions for creating vampire characters.

\autoref{disciplines}: Disciplines delineates the magical powers of the undead.

\autoref{rules}: Rules provides the basic means of resolving the characters' various actions.

\autoref{systemsAndDrama}: Systems describes a plethora of ways to simulate everything from gentle seduction to brutal combat.

\autoref{history}: A History of the Kindred recounts the ancient history and bloody feuds among the Children of Caine.

\autoref{storytelling}: Storytelling tells Storytellers how to build entertaining stories in which to involve the characters.

\autoref{antagonists}: Antagonists gives notes on the Kindred's (few) friends and (many) enemies.

Finally, the Appendix provides addenda and rules for advanced players.

\subsection{Live-Action}

Most Vampire games take place around a tabletop, and the players describe what their characters say and do.
However, games can also be conducted through Live-action play.
This exciting form of gaming bears similarities to improvisational theatre, in that players actually dress as their characters and act out their characters' scenes as though they were actors in a play.
Thus, rather than saying, ``My character walks over to the table and picks up the ancient document,'' you, the player, would actually get up, walk over to a properly decorated table, and pick up the ``ancient document'' (probably a prop created by the Storyteller -- for example, a piece of parchment that's been scorched around the edges to give it the appearance of age with a little flour ``dust'').

A Storyteller still guides the action and directs the plot; the Storyteller describes special features of the setting, oversees challenges the characters undergo, and may interrupt the action at any time.

Live-action roleplaying does not typically use dice; alternate systems, such as those presented in White Wolfs Mind's Eye Theatre line of products, take the place of dice when determining the results of challenges.
Most situations are resolved simply through acting and the Storyteller's decisions.

\subsubsection{Safeguards}

Some rules are necessary to ensure that live-action is safe and enjoyable for all participants and bystanders.
Unlike any other rules in this book, these rules must be followed.

-- No Touching: Period.
All combat and physical interaction must be handled through dice or other abstract systems.
Players must never strike, grapple or otherwise touch anyone during the game.
It is the Storyteller's responsibility to call a time-out if one or more players get overly rambunctious.

-- No Weapons: Props such as hats, period dress and canes are great in a live-action game.
Weapons aren't.
Period.
No knives, no swords and nothing that even remotely resembles a firearm.
Don't even bring fake swords, squirt guns, or foam-rubber weapons.
If your character must carry a ``weapon,'' take a 3'' x 5'' card and write ``Gun'' or ``Sword'' or whatever on it;
during combat challenges, present the card to the Storyteller, who will adjudicate its use in play.

-- Play in a Designated Area: Live-action is meant to be played in the home or other predesignated spot.
Don't involve bystanders in the game, and make sure everyone in the area, or who passes through the area, understands exactly what you're doing.
A game can look disturbing, even frightening, to those who aren't aware of what's going on.
Don't try to shock or intimidate passersby; this is not only immature, but could also lead to well-deserved prosecution.

-- Know When to Stop: If the Storyteller calls for a time-out or other break in the action, stop immediately.
The Storyteller is still the final arbiter of all events in the game.
Likewise, when the game is over for the night, take out your fangs and call it a night.

-- It's Only a Game: Live-action is for having fun.
If a rival wins, if a character dies, if a plan goes awry, it's not the end of the world.
Sometimes folks like to get together outside the game and talk about it -- say, a group of players who form a neonate coterie gathers to complain about their sires over pizza -- and there's nothing wrong with that.
But calling your clanmate up at four in the morning to ask for her assistance in your primogen bid is taking things too far.
Remember, everyone's doing this to have fun!

The Bottom Line: Live-action can be one of the richest and most satisfying storytelling experiences, if handled maturely and responsibly.
We're not kidding about that ``maturely and responsibly,'' folks.
In live-action, you are the prop, so it is imperative that you treat yourself and others with utmost care, dignity and respect.
This game is emphatically not about
``real'' blood-drinking, hunting, fighting or erotic activities.
You are not a vampire, you only play one in the game.

\section{Source Material}

Vampire, of course, pays homage to a long-standing and thriving genre.
The vampire/goth subculture waxes and wanes in the public eye, but is perennially alive (or undead) and kicking.
The following are a few important influences on Vampire:
The Masquerade and the World of Darkness.

Recommended literature includes: Dracula, by Bram Stoker; Interview with the Vampire, The Vampire Lestat, and The
Queen of the Damned, by Anne Rice; Lost Souts, by Poppy Z.
Brite; Brian Lumley's Necroscope series; The Hunger, by
Whitley Streiber; and I Am Legend, by Richard Matheson.
The vampire plays a role in the Romantic poetry of Byron, Shelley and Baudelaire.
As well, cruise on down to a public or university library and read some of the scary old myths and legends of vampires around the world.

The vampire has also played a role in film.
Bela Lugosi's Dracuia and Murnau's silent Nosferatu are the granddaddies of the genre.
Other good (or at least amusing) films include The Hunger, Near Dark, Vamp, The Lost Boys, Salem's Lot, the
Cristopher Lee Hammer Horror films, and the anime flick Vampire Hunter D.
Coppola's Dracula is not the best in terms of plot, but does have lush cinematography.
For camp, Buffy the Vampire Slayer and From Dusk Till Dawn provide some entertaining moments.

In terms of capturing the ambience of the World of Darkness, try Blade Runner, Tim Burton's Batman (first film only), The
Silence of the Lambs, Trainspotting, New Jack City, and most Hitchcock films.

\end{multicols}
